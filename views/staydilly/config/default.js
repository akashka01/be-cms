'use strict';

/* eslint comma-dangle:[0, "only-multiline"] */
var client = 'staydilly';

module.exports = {
    client: {
        lib: {
            css: [
                // bower:css
                'public/lib/bootstrap/dist/css/bootstrap.css',
                'public/lib/bootstrap/dist/css/bootstrap-theme.css',
                'public/lib/angular-ui-notification/dist/angular-ui-notification.css',
                'public/lib/angular-ui-select/dist/select.min.css',
                'views/'+client+'/modules/core/client/custom/style.css',
                'views/'+client+'/modules/core/client/custom/menu.css',
                'views/'+client+'/modules/core/client/custom/responsive.css',
                'views/'+client+'/modules/core/client/custom/angular-lightbox.css',
                'views/'+client+'/modules/core/client/custom/layerslider.css',
                'views/'+client+'/modules/core/client/custom/head_nav.css',
                'views/'+client+'/modules/core/client/custom/room_details_page.css',
                'views/'+client+'/modules/core/client/custom/hotel-list-page.css',
                'views/'+client+'/modules/core/client/custom/about.css',
                'views/'+client+'/modules/core/client/custom/grey.css',
                'views/'+client+'/modules/core/client/custom/customresponsive.css'
              // endbower
            ],
            js: [
                // bower:js
                'public/lib/jquery/dist/jquery.min.js',
                '/public/lib/bootstrap/dist/js/bootstrap.min.js',
                'public/lib/angular/angular.js',
                'public/lib/angular-animate/angular-animate.js',
                'public/lib/ng-file-upload/ng-file-upload.js',
                'public/lib/angular-messages/angular-messages.js',
                'public/lib/angular-mocks/angular-mocks.js',
                'public/lib/angular-resource/angular-resource.js',
                'public/lib/angular-ui-notification/dist/angular-ui-notification.js',
                'public/lib/angular-ui-router/release/angular-ui-router.js',
                'public/lib/owasp-password-strength-test/owasp-password-strength-test.js',
                'public/lib/angular-recaptcha/release/angular-recaptcha.js',
                'public/lib/lodash/dist/lodash.min.js',
                'public/lib/jquery-migrate/jquery-migrate.min.js',
                'public/lib/ngMeta/dist/ngMeta.min.js',
                'public/lib/angular-sanitize/angular-sanitize.min.js',
                'public/lib/angular-cookies/angular-cookies.min.js',
                'public/lib/angular-ui-select/dist/select.min.js',
                'public/lib/angular-toastr/dist/angular-toastr.js',
                'public/lib/angular-wysiwyg/dist/angular-wysiwyg.min.js',
                'public/lib/angular-wysiwyg/demo/scripts/bootstrap-colorpicker-module.js',
                'public/lib/moment/min/moment.min.js',
                'public/lib/angular-responsive-images/js/angular-responsive-images.min.js'
              // endbower
            ],
            tests: ['public/lib/angular-mocks/angular-mocks.js']
        },
        css: [
            'modules/*/client/{css,less,scss}/*.css',
            'views/' + client + '/modules/*/client/{css,less,scss}/*.css'
        ],
        less: [
            'modules/*/client/less/*.less'
        ],
        sass: [
            'modules/*/client/scss/*.scss'
        ],
        js: [
            'modules/core/client/app/config.js',
            'modules/core/client/app/init.js',
            'modules/*/client/*.js',
            'modules/*/client/**/*.js',
            'views/' + client + '/modules/*/client/js/*.js'

        ],
        img: [
            'modules/**/*/img/**/*.jpg',
            'modules/**/*/img/**/*.png',
            'modules/**/*/img/**/*.gif',
            'modules/**/*/img/**/*.svg'
        ],
        views: ['views/' + client + '/modules/*/client/views/**/*.html'],
        templates: ['build/templates.js']
    },
    server: {
        gulpConfig: ['gulpfile.js'],
        allJS: ['server.js', 'config/**/*.js', 'modules/*/server/**/*.js'],
        models: 'modules/*/server/models/**/*.js',
        routes: ['modules/!(core)/server/routes/**/*.js', 'modules/core/server/routes/**/*.js'],
        sockets: 'modules/*/server/sockets/**/*.js',
        config: ['modules/*/server/config/*.js'],
        policies: 'modules/*/server/policies/*.js',
        views: ['modules/*/server/views/*.html']
    }
};
