'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  config = require(path.resolve('./config/config'));

/**
 * Module init function.
 */
module.exports = function configuration() {

  return {
    project: 'staydilly',
    apiKey: '846d2f9159ffd2b80ff6a7cbd80ebf83a633a2b6',
    secretKey: 'sTaydILly',
    channelId: 3,
    url: 'http://staydilly.axisrooms.com/api/be',
    searchApi: '/search',
    locationApi: '/location',
    propertytypeApi: '/propertyType',
    roomApi: '/rooms',
    bookingApi: '/booking',
    dealApi: '/deals',
    promoApi: '/promos',
    addUserApi: '/userRegistration',
    buyerLoginApi: '/buyerLogin',
    buyerBookingHistoryApi: '/buyerBookingHistory',
    getBookingDetailsApi: '/orderDetails',
    cancelBookingApi: '/cancelAmtRequest',
    walletDetailsApi: '/getWalletDetails',
    addWalletAmountApi: '/addWalletAmount',
    updateUserProfileApi: '/updateProfile',
    updateUserPasswordAPi: '/updatePassword',
    validpromoApi: '/validatePromo',
    referAfriendAPi: '/referFrienedRequest',
    beId: '?bookingEngineId=1931',
    bookingEngineId: 1931,
    captchaSecretKey: '6LfyiAoUAAAAAOeg18oRpcs8YUgt4ul9cazwIY10',
    googleMapkey: 'AIzaSyDk163UPWaIzp6mX5CtSuN4CcQQlbBV7hc',
    facebookId: '219416175139101',
    tawkChartId: '57d931aa3bec6867d9461ff9',
    GoogleAnalyticsId: 'UA-84799425-1',
    FacebookAnalyticsId: '1812309345703923',
    reviewsApi: 'http://ec2-52-220-173-36.ap-southeast-1.compute.amazonaws.com/api/gethotelreviews/?hotel_ids=',
    fullStoryFlag: true,
    siteName: 'Staydilly'
  };

};
