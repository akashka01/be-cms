(function (app) {
  'use strict';

  app.registerModule('config');

  angular
    .module('config')
    .constant('CLIENT', {
      'name': 'traveler.mv',
      'capitalizeName': 'Traveller'
    })
  	.run(['ngMeta', function(ngMeta) {
    	ngMeta.init();
  	}]);

}(ApplicationConfiguration));