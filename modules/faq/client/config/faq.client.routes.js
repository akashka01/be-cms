(function() {
    'use strict';

    angular
        .module('faq.routes')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider', 'CLIENT'];

    function routeConfig($stateProvider, CLIENT) {
        $stateProvider
            .state('faq', {
                url: '/faq',
                templateUrl: '/views/' + CLIENT.name + '/modules/faq/client/views/faq.client.view.html',
                controller: 'faqController',
                data: {
                  meta: {
                    'title': CLIENT.capitalizeName + ' | Frequently Asked Questions',
                    'description': 'Find answers to the questions we get asked the most on Staydilly.',
                    'keywords': ''
                  }
                }
            });
    }
}());
