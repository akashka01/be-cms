(function () {
  'use strict';

  angular
    .module('users')
    .controller('referAFriendController', referAFriendController);

  referAFriendController.$inject = ['$scope', '$state', '$http', '$window', 'Authentication',
    'Notification', '$rootScope', '$timeout', '$stateParams'];

  function referAFriendController($scope, $state, $http, $window, Authentication, Notification, $rootScope, $timeout, $stateParams) {

    var params = {
      headers: { "params": JSON.stringify($stateParams) }
    };
    $http.get('/api/getWalletDetails', params).success(function(res) {
      $rootScope.userDeatails = res;
      console.log(res);
    });
    $scope.refer = function(referEmail) {
      referEmail.userId = $rootScope.userDeatails.user_id;
      var params = {
        headers: { "params": JSON.stringify(referEmail) }
      };
      $http.get('/api/referAFriend', params).success(function(res) {

        $scope.refferd = res;
        if ($scope.refferd) {
          console.log("success");
        } else {

        }
        console.log(res);
      });
    }
  }
}());
