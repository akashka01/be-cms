(function () {
  'use strict';

  angular
    .module('users')
    .controller('UserDetailsController', UserDetailsController);

  UserDetailsController.$inject = ['$scope', '$rootScope', 'patterns', '$stateParams', '$http', '$timeout', 'CLIENT'];

  function UserDetailsController($scope, $rootScope, patterns, $stateParams, $http, $timeout, CLIENT) {
    $scope.dataReceived = false;
    $timeout(function() {
      (CLIENT.name == 'staydilly') ? window.scrollTo(0, 130) : "";
    }, 100);

    $scope.editProfile= false;
    $scope.editUser = function(){
      $scope.editProfile= true;
    };
    $scope.profileUser = function(){
      $scope.editProfile= false;
    };

    $scope.patterns = patterns;
    var params = {
      headers: { "params": JSON.stringify($stateParams) }
    };
    $http.get('/api/getWalletDetails', params).success(function(res) {
      $rootScope.userDeatails = res;
      $scope.dataReceived = true;
    });

    $scope.updateUser = function(userUpdate) {
        userUpdate.walletLog = [];
      $http.post('/api/updateUserProfile', userUpdate).success(function(res) {
      });
    }
  }
}());
