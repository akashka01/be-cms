(function () {
  'use strict';

  angular
    .module('users')
    .controller('changePasswordController', ChangePasswordController);

  ChangePasswordController.$inject = ['$scope', '$http', 'Authentication', 'UsersService', 'PasswordValidator', 'Notification',
     '$rootScope', '$timeout'];

  function ChangePasswordController($scope, $http, Authentication, UsersService, PasswordValidator, Notification,
                                    $rootScope, $timeout) {

    $scope.updatepassword = function(updatePass) {
      updatePass.emailId = $rootScope.userDeatails.email;
      var params = {
        headers: { "params": JSON.stringify(updatePass) }
      };
      $http.get('/api/updateUserPassword', params).success(function(res) {
        if (res.Result != undefined) {

        } else if (res.Error != undefined) {
          $scope.errorMeassege = res.Error.ErrorMessage;
        } else {
          $scope.errorMeassege = res.Error.ErrorMessage;
        }

      });
    }
  }
}());
