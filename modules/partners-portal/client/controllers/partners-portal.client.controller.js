(function() {
    'use strict';

    angular
        .module('partnerPortal')
        .controller('partnerPortalController', partnerPortalController);

    partnerPortalController.$inject = ['$scope', 'Authentication', '$http', 'vcRecaptchaService', 'patterns','MetaTagsServices'];

    function partnerPortalController($scope, Authentication, $http, vcRecaptchaService, patterns, MetaTagsServices) {
      //to call meta tags for this page
      MetaTagsServices.metaTagHeader();

    }
}());
