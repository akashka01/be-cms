(function() {
    'use strict';

    angular
        .module('aboutUs')
        .controller('aboutUsController', aboutUsController);

    aboutUsController.$inject = ['$scope', 'Authentication', '$http', 'vcRecaptchaService', 'patterns','MetaTagsServices'];

    function aboutUsController($scope, Authentication, $http, vcRecaptchaService, patterns, MetaTagsServices) {
      //to call meta tags for this page
      MetaTagsServices.metaTagHeader();

    }
}());
