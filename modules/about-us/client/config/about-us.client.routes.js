(function() {
    'use strict';

    angular
        .module('aboutUs.routes')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider', 'CLIENT'];

    function routeConfig($stateProvider, CLIENT) {
        $stateProvider
            .state('aboutUs', {
                url: '/about-us',
                templateUrl: '/views/' + CLIENT.name + '/modules/about-us/client/views/about-us.client.view.html',
                controller: 'aboutUsController',
                data: {
                  meta: {
                    'title': CLIENT.capitalizeName + ' | About-Us',
                    'description': 'Revolutionize the travel industry. That is what we aim to do. How?',
                    'keywords': ''
                  }
                }
            });
    }
}());
