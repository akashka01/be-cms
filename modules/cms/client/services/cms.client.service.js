(function () {
  'use strict';

  angular
    .module('cms.services')
    .constant('HEADING',{
      deals: 'DEALS',
      collections: 'COLLECTIONS',
      locations: 'LOCATIONS',
      blogs: 'BLOGS',
      promotions: 'PROMOTIONS'
    })
    .factory('Hotels', Hotels)
    .factory('Deals', Deals)
    .factory('Blogs', Blogs)
    .factory('Collections', Collections)
    .factory('Heading', Heading)
    .factory('SelectedBlogs', SelectedBlogs)
    .factory('SelectedCollections', SelectedCollections)
    .factory('Locations', Locations)
    .factory('SelectedLocations', SelectedLocations)
    .factory('Promos', Promos)
    .factory('selectedPromos', SelectedPromos)
    .factory('Policy', Policy)




  Hotels.$inject = ['$resource', '$log', '$http'];
  function Hotels($resource, $log, $http) {
    return {
      get: function () {
        return $http.get('http://private-5b1ef-staydilly.apiary-mock.com/hotels');
      }
    }
  }
  Deals.$inject = ['$resource', '$log', '$http'];
  function Deals($resource, $log, $http) {
    return {
      get: function () {
        return $http.get('/api/getDeals');
      }
    }
  }
  Blogs.$inject = ['$resource', '$log', '$http'];
  function Blogs($resource, $log, $http) {
    return {
      get: function () {
        return $http.get('/api/blogs');
      },
      post: function(blogSelected){
        return $http.post('/api/selectedBlog', blogSelected);
      },
      put: function(blogSelected, blogId){
        return $http.put('/api/selectedBlog/' + blogId, blogSelected);
      }
    }
  }
  SelectedBlogs.$inject = ['$resource', '$log', '$http'];
  function SelectedBlogs($resource, $log, $http) {
    return {
      get: function () {
        return $http.get('/api/selectedBlogs');
      }
    }
  }
  Collections.$inject = ['$resource', '$log', '$http'];
  function Collections($resource, $log, $http) {
    return {
      get: function () {
        return $http.get('/api/collections');
      },
      post: function(collectionSelected){
        return $http.post('/api/selectedCollection', collectionSelected);
      },
      put: function(collectionSelected, collectionId){
        return $http.put('/api/selectedCollection/' + collectionId, collectionSelected);
      }
    }
  }
  SelectedCollections.$inject = ['$resource', '$log', '$http'];
  function SelectedCollections($resource, $log, $http) {
    return {
      get: function () {
        return $http.get('/api/selectedCollections');
      }
    }
  }
  Locations.$inject = ['$resource', '$log', '$http'];
  function Locations($resource, $log, $http) {
    return {
      get: function () {
        return $http.get('/api/locations');
      },
      post: function(selectedLocation){
        return $http.post('/api/selectedLocation', selectedLocation);
      },
      put: function(selectedLocation, locationId){
        return $http.put('/api/selectedLocation/' + locationId, selectedLocation);
      }
    }
  }
  SelectedLocations.$inject = ['$resource', '$log', '$http'];
  function SelectedLocations($resource, $log, $http) {
    return {
      get: function () {
        return $http.get('/api/selectedLocations');
      }
    }
  }
  Promos.$inject = ['$resource', '$log', '$http'];
  function Promos($resource, $log, $http) {
    return {
      get: function () {
        return $http.get('/api/promos');
      },
      post: function(selectedPromo){
        return $http.post('/api/selectedPromo', selectedPromo);
      },
      put: function(selectedPromo, promoId){
        return $http.put('/api/selectedPromo/' + promoId, selectedPromo);
      }
    }
  }
  SelectedPromos.$inject = ['$resource', '$log', '$http'];
  function SelectedPromos($resource, $log, $http) {
    return {
      get: function () {
        return $http.get('/api/selectedPromos');
      }
    }
  }
  Policy.$inject = ['$resource', '$log', '$http'];
  function Policy($resource, $log, $http) {
    return {
      get: function () {
        return $http.get('/api/policy');
      },
      post: function(policy){
        return $http.post('/api/policy', policy);
      },
      put: function(policy, policyId){
        return $http.put('/api/policy/' + policyId, policy);
      }
    }
  }

  Heading.$inject = ['$resource', '$log', '$http'];
  function Heading($resource, $log, $http) {
    return {
          get: function (headingName) {
            return $http.get('/api/heading/'+ headingName);
          },
          post: function(heading){
            return $http.post('/api/heading', heading);
          },
          put: function(heading, headingId){
            return $http.put('/api/heading/' + headingId, heading);
          }
        }
  }
}());
