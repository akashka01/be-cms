(function() {
    'use strict';

    angular
        .module('cms')
        .controller('dashboardController', dashboardController);

    dashboardController.$inject = ['$scope', 'Authentication', '$rootScope', '$http'];

    function dashboardController($scope, Authentication, $rootScope, $http) {
      $rootScope.page = 'dashboard';
    }
}());
