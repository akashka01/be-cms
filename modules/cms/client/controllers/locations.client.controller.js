(function() {
    'use strict';

    angular
        .module('cms')
        .controller('locationsController', locationsController);

    locationsController.$inject = ['$scope', 'Authentication', '$rootScope', '$http',
      'allCities', 'selectedLocations', 'Locations', 'toastr', 'HEADING', 'heading', '$uibModal', 'CLIENT'];

    function locationsController($scope, Authentication, $rootScope, $http,
                                 allCities, selectedLocations, Locations, toastr, HEADING, heading, $uibModal, CLIENT) {
      $scope.allCities = allCities.data.locations;
      $scope.locations = _.uniqBy($scope.allCities, 'state_id');

      $scope.rightSectionVisibility = false;
      $scope.currentIndex = 1;
      $scope.selectedLocation = {};
      $scope.myLocations = [];
      $scope.heading = heading.data ? heading.data : {'type': HEADING.locations};

      _(selectedLocations.data).forEach(function (location) {
        $scope.myLocations[location.position] = _.find(selectedLocations.data, {'state_id': location.location_id});
      });

      $scope.addLocation = function(sectionIndex){
        $scope.rightSectionVisibility = true;
        $scope.currentIndex = sectionIndex;
        $scope.editing = false;
      };

      $scope.editLocation = function(locationSectionIndex){
        $scope.rightSectionVisibility = true;
        $scope.currentIndex = locationSectionIndex;
        $scope.selectedLocation.id = $scope.myLocations[locationSectionIndex].state_id;
        $scope.selectedLocation.image = $scope.myLocations[locationSectionIndex].image;
        $scope.editing = true;
      };

      $scope.saveLocation = function () {
        var selectedLocation= {
          location_id: $scope.selectedLocation.id,
          position: $scope.currentIndex,
          description: $scope.selectedLocation.description,
          image: $scope.selectedLocation.image
        };

        Locations.post(selectedLocation).success(function (res) {
          $scope.myLocations[$scope.currentIndex] = res;
          $scope.rightSectionVisibility = false;
          $scope.selectedLocation = {};
          toastr.success('Saved Successfully');
        });
      };

      $scope.updateLocation = function () {
        var selectedLocation = {
          location_id: $scope.selectedLocation.id,
          position: $scope.currentIndex,
          description: $scope.selectedLocation.description,
          image: $scope.selectedLocation.image
        };

        Locations.put(selectedLocation, $scope.myLocations[$scope.currentIndex]._id).success(function (res) {
          $scope.myLocations[$scope.currentIndex] = res;
          $scope.rightSectionVisibility = false;
          $scope.selectedLocation = {};
          toastr.success('Updated Successfully');
        });
      };

      $scope.openImageModal = function(){
        var $ctrl = this;
        var modalInstance = $uibModal.open({
          animation: $ctrl.animationsEnabled,
          ariaLabelledBy: 'modal-title-bottom',
          ariaDescribedBy: 'modal-body-bottom',
          templateUrl: '/views/' + CLIENT.name + '/modules/cms/client/views/models/imageUploadModal/imageUploadModal.html',
          size: 'lg',
          controller: "ImageUploadController",
          resolve: {
            'image': function(){
              return $scope.selectedLocation.image;
            }
          }
        });

        modalInstance.result.then(function (resultImage) {
          $scope.selectedLocation.image = resultImage;
        });
      };
    }
}());
