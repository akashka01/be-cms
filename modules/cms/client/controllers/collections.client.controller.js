(function() {
    'use strict';

    angular
        .module('cms')
        .controller('collectionsController', collectionsController);

    collectionsController.$inject = ['$scope', 'Authentication', '$rootScope','toastr', '$http',
      'propertyTypes', 'selectedCollections', 'Collections', 'HEADING', 'heading',
      'Heading', '$uibModal', 'CLIENT'];

    function collectionsController($scope, Authentication, $rootScope, toastr, $http,
                                   propertyTypes, selectedCollections, Collections, HEADING, heading,
                                   Heading, $uibModal, CLIENT) {
      $scope.collections = propertyTypes.data.PropertyList;

      $scope.rightSectionVisibility = false;
      $scope.currentIndex = 1;
      $scope.selectedCollection = {};
      $scope.myCollections = [];
      $scope.heading = heading.data ? heading.data : {'type': HEADING.collections};
      _(selectedCollections.data).forEach(function (collection) {
        $scope.myCollections[collection.position] = _.find(selectedCollections.data, {'id': collection.collection_id});
      });

      $scope.addCollection = function(sectionIndex){
        $scope.rightSectionVisibility = true;
        $scope.currentIndex = sectionIndex;
        $scope.editing = false;
      };

      $scope.editCollection = function(collectionSectionIndex){
        $scope.rightSectionVisibility = true;
        $scope.currentIndex = collectionSectionIndex;
        $scope.selectedCollection.id = $scope.myCollections[collectionSectionIndex].id;
        $scope.selectedCollection.description = $scope.myCollections[collectionSectionIndex].description;
        $scope.selectedCollection.image = $scope.myCollections[collectionSectionIndex].image;
        $scope.editing = true;
      };

      $scope.saveCollection = function () {
        var selectedCollection= {
          collection_id: $scope.selectedCollection.id,
          position: $scope.currentIndex,
          description: $scope.selectedCollection.description,
          image: $scope.selectedCollection.image
        };

        Collections.post(selectedCollection).success(function(res) {
          $scope.myCollections[$scope.currentIndex] = res;
          $scope.rightSectionVisibility = false;
          $scope.selectedCollection = {};
          toastr.success('Saved Successfully');
        });
      };

      $scope.updateCollection = function () {
        var selectedCollection = {
          collection_id: $scope.selectedCollection.id,
          position: $scope.currentIndex,
          description: $scope.selectedCollection.description,
          image: $scope.selectedCollection.image
        };
        Collections.put(selectedCollection, $scope.myCollections[$scope.currentIndex]._id).success(function(res) {
          $scope.myCollections[$scope.currentIndex] = res;
          $scope.rightSectionVisibility = false;
          $scope.selectedCollection = {};
          toastr.success('Updated Successfully');
        });
      };


      $scope.openImageModal = function(){
        var $ctrl = this;
        var modalInstance = $uibModal.open({
          animation: $ctrl.animationsEnabled,
          ariaLabelledBy: 'modal-title-bottom',
          ariaDescribedBy: 'modal-body-bottom',
          templateUrl: '/views/' + CLIENT.name + '/modules/cms/client/views/models/imageUploadModal/imageUploadModal.html',
          size: 'lg',
          controller: "ImageUploadController",
          resolve: {
            'image': function(){
              return $scope.selectedCollection.image;
            }
          }
        });

        modalInstance.result.then(function (resultImage) {
          $scope.selectedCollection.image = resultImage;
        });
      };
    }
}());
