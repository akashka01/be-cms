(function() {
    'use strict';

    angular
        .module('cms')
        .controller('policiesController', policiesController);

    policiesController.$inject = ['$scope', 'Authentication', '$rootScope', '$http',
      'policy', 'Policy', 'toastr'];

    function policiesController($scope, Authentication, $rootScope, $http,
                                policy, Policy, toastr) {
      $scope.policy = policy.data ? policy.data[0] : {};
      if($scope.policy != undefined) {
        $scope.policy.customMenu = [
          ['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'],
          ['format-block'],
          ['font'],
          ['font-size'],
          ['font-color', 'hilite-color'],
          ['remove-format'],
          ['ordered-list', 'unordered-list', 'outdent', 'indent'],
          ['left-justify', 'center-justify', 'right-justify'],
          ['code', 'quote', 'paragraph'],
          ['link', 'image']
        ];
      }


      $scope.rightSectionVisibility = false;
      $scope.currentIndex = 1;

      $scope.savePolicy = function(){
        if($scope.policy._id){
          updatePolicy();
        }else{
          createPolicy();
        }
      };

      function createPolicy() {
        var selectedPolicy= {
          text: $scope.policy.text
        };

        Policy.post(selectedPolicy).success(function (res) {
          $scope.rightSectionVisibility = false;
          toastr.success('Saved Successfully');
        });
      }

      function updatePolicy() {
        var selectedPolicy = {
          text: $scope.policy.text
        };

        Policy.put(selectedPolicy, $scope.policy._id).success(function (res) {
          $scope.rightSectionVisibility = false;
          toastr.success('Updated Successfully');
        });
      }
    }
}());
