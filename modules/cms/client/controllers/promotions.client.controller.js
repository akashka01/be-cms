(function() {
    'use strict';

    angular
        .module('cms')
        .controller('promotionsController', promotionsController);

    promotionsController.$inject = ['$scope', 'Authentication', '$rootScope', '$http',
       'selectedPromos', 'Promos', 'toastr', 'HEADING', 'heading'];

    function promotionsController($scope, Authentication, $rootScope, $http,
                                   selectedPromos, Promos, toastr, HEADING, heading) {
      //$scope.promos = promos.data.promos;

      $scope.rightSectionVisibility = false;
      $scope.currentIndex = 1;
      $scope.selectedPromo = {};
      $scope.myPromos = [];
      $scope.heading = heading.data ? heading.data : {'type': HEADING.promotions};

      _(selectedPromos.data).forEach(function (promo) {
        $scope.myPromos[promo.position] = _.find(selectedPromos.data, {'id': promo.promo_id});
      });

      $scope.addPromo = function(sectionIndex){
        $scope.rightSectionVisibility = true;
        $scope.currentIndex = sectionIndex;
        $scope.editing = false;
      };

      $scope.editPromo = function(promoSectionIndex){
        $scope.rightSectionVisibility = true;
        $scope.currentIndex = promoSectionIndex;
        $scope.selectedPromo.id = $scope.myPromos[promoSectionIndex].id;
        $scope.selectedPromo.title = $scope.myPromos[promoSectionIndex].title;
        $scope.editing = true;
      };

      $scope.savePromo = function () {
        var selectedPromo= {
          promo_id: $scope.selectedPromo.id,
          position: $scope.currentIndex,
          description: $scope.selectedPromo.description,
          url: 'demo.jpg',
          title: $scope.selectedPromo.title
        };

        Promos.post(selectedPromo).success(function (res) {
          $scope.myPromos[$scope.currentIndex] = res;
          $scope.rightSectionVisibility = false;
          $scope.selectedPromo = {};
          toastr.success('Saved Successfully');
        });
      };

      $scope.updatePromo = function () {
        var selectedPromo = {
          promo_id: $scope.selectedPromo.id,
          position: $scope.currentIndex,
          description: $scope.selectedPromo.description,
          url: 'demo.jpg',
          title: $scope.selectedPromo.title
        };

        Promos.put(selectedPromo, $scope.myPromos[$scope.currentIndex]._id).success(function (res) {
          $scope.myPromos[$scope.currentIndex] = res;
          $scope.rightSectionVisibility = false;
          $scope.selectedPromo = {};
          toastr.success('Updated Successfully');
        });
      };
    }
}());
