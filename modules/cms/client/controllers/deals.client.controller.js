(function() {
    'use strict';

    angular
        .module('cms')
        .controller('dealsController', dealsController);

    dealsController.$inject = ['$scope', 'Authentication', '$rootScope', '$http',
      'dealsData', 'myDeals', 'toastr', 'heading', 'HEADING'];

    function dealsController($scope, Authentication, $rootScope, $http,
                             dealsData, myDeals, toastr, heading, HEADING) {

      $rootScope.page = 'deals';
      $scope.deals = dealsData.data.dealsList;
      $scope.myDeals = myDeals.data;
      $scope.showPanel = false;
      $scope.heading = heading.data ? heading.data : {'type': HEADING.deals};

      $scope.selectedDeals = {};

      _($scope.myDeals).forEach(function (value) {
        $scope.selectedDeals[value.position] = _.find($scope.deals, ['id', value.deal_id]);
      });

      $scope.togglePanel = function (currentPanel, editing) {
        $scope.editing = false;
        $scope.deal = '';

        $scope.showPanel = true;
        $scope.currentPanel = currentPanel;
        if (editing) {
          $scope.editing = editing;
          $scope.deal = $scope.selectedDeals[currentPanel];
        }
      };

      $scope.setItem = function (item) {
        $scope.deal = item;
      };

      $scope.add = function (deal) {
        var dealSelected = {
          deal_id: deal.id,
          image: 'image',
          position: $scope.currentPanel
        };

        $http.post('/api/deals', dealSelected).success(function (res) {
          $scope.selectedDeals[$scope.currentPanel] = _.find($scope.deals, ['id', res.deal_id]);
          toastr.success('Saved Successfully');
        })
      };

      $scope.update = function (deal) {
        var dealSelected = {
          image: 'image updated',
          deal_id: deal.id
        };

        $http.put('/api/deals/' + $scope.currentPanel, dealSelected).success(function (res) {
          $scope.selectedDeals[$scope.currentPanel] = _.find($scope.deals, ['id', res.deal_id]);
          toastr.success('Updated Successfully');
        })
      };
    }
}());
