(function() {
    'use strict';

    angular
        .module('cms.routes')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider', 'CLIENT'];

  function routeConfig($stateProvider, CLIENT) {
        $stateProvider
            .state('cms', {
                url: '/admin',
                templateUrl: '/views/' + CLIENT.name + '/modules/cms/client/views/cms.client.view.html',
                controller: 'cmsController',
                controllerAs: 'vm',
                data: {
                  roles: ['admin'],
                  meta: {
                    'title': CLIENT.capitalizeName + ' | Content Management System'
                  }
                }
            })
            .state('cms.dealsCms', {
              url: '/deals',
              templateUrl: '/views/' + CLIENT.name + '/modules/cms/client/views/deals.client.view.html',
              controller: 'dealsController',
              controllerAs: 'vm',
              resolve: {
                heading: ['Heading', 'HEADING', function(Heading, HEADING){
                  return Heading.get(HEADING.deals);
                }],
                dealsData: function (Deals) {
                  return Deals.get();
                },
                myDeals: function ($http) {
                  return $http.get('/api/deals');
                }
              }
            })
            .state('cms.collections', {
              url: '/collections',
              templateUrl: '/views/' + CLIENT.name + '/modules/cms/client/views/collections.client.view.html',
              controller: 'collectionsController',
              controllerAs: 'vm',
              resolve: {
                heading: ['Heading', 'HEADING', function(Heading, HEADING){
                  return Heading.get(HEADING.collections);
                }],
                propertyTypes: ['$http', function($http){
                  return $http.get('/api/getPropertytype').success(function(res) {
                    return res.PropertyList;
                  });
                }],
                selectedCollections: ['SelectedCollections', function (SelectedCollections) {
                  return SelectedCollections.get();
                }]
              }
            })
            .state('cms.dashboard', {
              url: '/dashboard',
              templateUrl: '/views/' + CLIENT.name + '/modules/cms/client/views/dashboard.client.view.html',
              controller: 'dashboardController',
              controllerAs: 'vm'
            })
            .state('cms.blog', {
              url: '/blog',
              templateUrl: '/views/' + CLIENT.name + '/modules/cms/client/views/blog.client.view.html',
              controller: 'blogController',
              controllerAs: 'vm',
              resolve: {
                heading: ['Heading', 'HEADING', function(Heading, HEADING){
                  return Heading.get(HEADING.blogs);
                }],
                blogsData: function (Blogs) {
                  return Blogs.get();
                },
                selectedBlogs: function (SelectedBlogs) {
                  return [];
                }
              }
            })
            .state('cms.locations', {
              url: '/locations',
              templateUrl: '/views/' + CLIENT.name + '/modules/cms/client/views/locations.client.view.html',
              controller: 'locationsController',
              controllerAs: 'vm',
              resolve: {
                heading: ['Heading', 'HEADING', function(Heading, HEADING){
                  return Heading.get(HEADING.locations);
                }],
                allCities: ['$http', function($http){
                  return $http.get('/api/getLocations').success(function(loc) {
                    return loc;
                  });
                }],
                selectedLocations: ['SelectedLocations', function (SelectedLocations) {
                  return SelectedLocations.get();
                }]
              }
            })
            .state('cms.hotels', {
              url: '/hotels',
              templateUrl: '/views/' + CLIENT.name + '/modules/cms/client/views/hotels.client.view.html',
              controller: 'hotelsCmsController',
              controllerAs: 'vm',
              resolve: {
                hotelsData: function (Hotels) {
                  return Hotels.get();
                }
              }
            })
          .state('cms.policies', {
            url: '/policies',
            templateUrl: '/views/' + CLIENT.name + '/modules/cms/client/views/policies.client.view.html',
            controller: 'policiesController',
            controllerAs: 'vm',
            resolve: {
              css: function ($http) {
                return $http.get('/api/css');
              },
              policy: ['Policy', function (Policy) {
                return Policy.get();
              }]
            }
          })
            .state('cms.promotion', {
              url: '/promotion',
              templateUrl: '/views/' + CLIENT.name + '/modules/cms/client/views/promotions.client.view.html',
              controller: 'promotionsController',
              controllerAs: 'vm',
              resolve: {
                heading: ['Heading', 'HEADING', function(Heading, HEADING){
                  return Heading.get(HEADING.promotions);
                }],
                promos: ['$http', function($http){
                  return $http.get('/api/getPromos').success(function(promos) {
                    return promos;
                  });
                }]
                //selectedPromos: ['SelectedPromos', function (SelectedPromos) {
                //  return SelectedPromos.get();
                //}]
              }
            })
            .state('cms.editor', {
              url: '/editor',
              templateUrl: '/views/' + CLIENT.name + '/modules/cms/client/views/editor.client.view.html',
              controller: 'editorController',
              controllerAs: 'vm',
              resolve: {
                css: function ($http) {
                  return $http.get('/api/css');
                }
              }
            });
    }
}());
