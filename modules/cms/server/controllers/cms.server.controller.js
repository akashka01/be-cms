'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Collection = mongoose.model('Collection'),
  Heading = mongoose.model('Heading'),
  Deal = mongoose.model('Deal'),
  Location = mongoose.model('Location'),
  Blog = mongoose.model('Blog'),
  Policy = mongoose.model('Policy'),
  Promo = mongoose.model('Promo'),


    request = require('request'),
  projectConfig = require(path.resolve('./modules/core/server/config/config')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

var _ = require('lodash');

var config = projectConfig();

var headers = {
  apiKey: config.apiKey,
  channelId: config.channelId
};

exports.getSelectedBlogs = function(req, res) {
  var allBlogs = [];
  var selectedBlogs = [];
  var options = {
    url: "http://axisrooms.website/staydillyblog/?json=get_posts",
    method: 'GET'
  };
  // Start the request
  request(options, function(error, response, body) {
    console.log(response.statusCode);
    if (!error && response.statusCode == 200) {

      Blog.find({}).exec(function(err, blogs) {
        if (err) return res.status(400).send(err);
        else{
          allBlogs = JSON.parse(body);
          _(blogs).forEach(function (blog) {
            selectedBlogs[blog.position] = _.find(allBlogs.posts, function(post){
              post._id = blog._id;
              post.blog_id = blog.blog_id;
              post.position = blog.position;
              return post.id == blog.blog_id
            });

          });
          res.send(_.compact(selectedBlogs));
        }
      });
    }
  });

};

exports.setSelectedBlog = function(req, res) {
  var blog = new Blog(req.body);
  blog.save(function(err) {
    if (err) return res.status(400).send(err);
    else res.jsonp(blog);
  })
};

exports.updateSelectedBlogs = function(req, res) {
  Blog.findOneAndUpdate({'_id': req.params.blogId}, {$set: req.body}, {new: true}, function(err, updatedBlog) {
    if (err) return res.status(400).send(err);
    else res.jsonp(updatedBlog);
  });
};


// Collections
exports.getSelectedCollections = function(req, res) {

  var propertyTypes = [];
  var selectedCollections = [];
  // Configure the request
  var options = {
    url: config.url + config.propertytypeApi,
    method: 'GET',
    headers: headers
  };


  // Start the request
  request(options, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      propertyTypes = JSON.parse(body);
      Collection.find({}).exec(function(err, collections) {
        if (err) return res.status(400).send(err);
        else {
          _(collections).forEach(function (collection) {
            selectedCollections[collection.position] = _.find(propertyTypes.PropertyList, {'id': collection.collection_id});
            selectedCollections[collection.position]._id = collection._id;
            selectedCollections[collection.position].collection_id = collection.collection_id;
            selectedCollections[collection.position].description = collection.description;
            selectedCollections[collection.position].image = collection.image;
            selectedCollections[collection.position].position = collection.position;
          });
          res.send(_.compact(selectedCollections));
        }
      });
    }
  })

};

exports.setSelectedCollection = function(req, res) {
  var collection = new Collection(req.body);
  collection.save(function(err) {
    if (err) return res.status(400).send(err);
    else res.jsonp(collection);
  })
};

exports.updateSelectedCollection = function(req, res) {
  Collection.findOneAndUpdate({ '_id': req.params.collectionId }, { $set: req.body }, { new: true }, function(err, updatedCollection) {
    if (err) return res.status(400).send(err);
    else res.jsonp(updatedCollection);
  });
};

// deals
exports.getMyDeals = function(req, res) {
  Deal.find({}).exec(function(err, deals) {
    if (err) return res.status(400).send(err);
    else res.jsonp(deals);
  });
};

exports.setMyDeals = function(req, res) {
  var deal = new Deal(req.body);

  deal.save(function(err) {
    if (err) return res.status(400).send(err);
    else res.jsonp(deal);
  })
};

exports.updateMyDeal = function(req, res) {
  console.warn(req);
  var deal = req.deal;
  deal = _.extend(deal, req.body);
  deal.save(function(err) {
    if (err) return res.status(400).send(err);
    else res.jsonp(deal);
  })
};

exports.dealByID = function(req, res, next, id) {
  var query = Deal.findOne({ position: id });
  query.exec(function(err, deal) {
    if (err) return next(err);
    else if (!deal) return res.status(404).send(err);
    req.deal = deal;
    next();
  });
};

exports.getDeals = function(req, res) {
  // Configure the request
  var options = {
    url: config.url + config.dealApi,
    method: 'GET',
    headers: headers
  };

  console.log(options.url);
  // Start the request
  request(options, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      res.json(JSON.parse(body));
    }
  })
};


//locations


exports.getSelectedLocations = function(req, res) {
  var allCities = [];
  var selectedLocations = [];
  var headers = {
    apiKey: config.apiKey,
    channelId: config.channelId
  };
  var options = {
    url: config.url + config.locationApi,
    method: 'GET',
    headers: headers
  };
  request(options, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      allCities = JSON.parse(body).locations;
      Location.find({}).exec(function(err, locations) {
        if (err) return res.status(400).send(err);
        else {
          _(locations).forEach(function (location) {
            selectedLocations[location.position] = _.find(allCities, {'state_id': location.location_id});
            selectedLocations[location.position]._id = location._id;
            selectedLocations[location.position].location_id = location.location_id;
            selectedLocations[location.position].image = location.image;
            selectedLocations[location.position].position = location.position;
          });
          res.send(_.compact(selectedLocations));
        }
      });
    }
  })


};

exports.setSelectedLocation = function(req, res) {
  var location = new Location(req.body);
  location.save(function(err) {
    if (err) return res.status(400).send(err);
    else res.jsonp(location);
  })
};

exports.updateSelectedLocation = function(req, res) {
  Location.findOneAndUpdate({'_id': req.params.locationId}, {$set: req.body}, {new: true}, function(err, updatedLocation) {
    if (err) return res.status(400).send(err);
    else res.jsonp(updatedLocation);
  });
};


exports.setSelectedPromo = function(req, res) {
  var promo = new Promo(req.body);
  promo.save(function(err) {
    if (err) return res.status(400).send(err);
    else res.jsonp(promo);
  })
};

exports.updateSelectedPromo = function(req, res) {
  Promo.findOneAndUpdate({'_id': req.params.promoId}, {$set: req.body}, {new: true}, function(err, updatedpromo) {
    if (err) return res.status(400).send(err);
    else res.jsonp(updatedpromo);
  });
};

// Policy
exports.getPolicy = function(req, res) {
  Policy.find({}).exec(function(err, policy) {
    if (err) return res.status(400).send(err);
    else res.jsonp(policy);
  });
};

exports.setPolicy = function(req, res) {
  var policy = new Policy(req.body);
  policy.save(function(err) {
    if (err) return res.status(400).send(err);
    else res.jsonp(policy);
  })
};

exports.updatePolicy = function(req, res) {
  Policy.findOneAndUpdate({'_id': req.params.policyId}, {$set: req.body}, {new: true}, function(err, updatedPolicy) {
    if (err) return res.status(400).send(err);
    else res.jsonp(updatedPolicy);
  });
};


//promos

exports.getPromos = function(req, res) {
  var options = {
    url: config.url + config.promoApi,
    method: 'GET',
    headers: headers
  };
  request(options, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      res.json(JSON.parse(body));
    }
  });
};

exports.getSelectedPromos = function(req, res) {
  var allPromos = [];
  var selectedPromos = [];
  var options = {
    url: config.url + config.promoApi,
    method: 'GET',
    headers: headers
  };
  request(options, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      allPromos = JSON.parse(body).promos;
      Promo.find({}).exec(function(err, promos) {
        if (err) return res.status(400).send(err);
        else {
          _(promos).forEach(function (promo) {
            selectedPromos[promo.position] = _.find(allPromos, {'id': promo.promo_id});
            selectedPromos[promo.position]._id = promo._id;
            selectedPromos[promo.position].promo_id = promo.promo_id;
            selectedPromos[promo.position].url = promo.url;
            selectedPromos[promo.position].title = promo.title;
            selectedPromos[promo.position].position = promo.position;
          });
          res.send(_.compact(selectedPromos));
        }
      });
    }
  });
};

exports.setSelectedPromo = function(req, res) {
  var promo = new Promo(req.body);
  promo.save(function(err) {
    if (err) return res.status(400).send(err);
    else res.jsonp(promo);
  })
};

exports.updateSelectedPromo = function(req, res) {
  Promo.findOneAndUpdate({'_id': req.params.promoId}, {$set: req.body}, {new: true}, function(err, updatedpromo) {
    if (err) return res.status(400).send(err);
    else res.jsonp(updatedpromo);
  });
};

// Policy
exports.getPolicy = function(req, res) {
  Policy.find({}).exec(function(err, policy) {
    if (err) return res.status(400).send(err);
    else res.jsonp(policy);
  });
};

exports.setPolicy = function(req, res) {
  var policy = new Policy(req.body);
  policy.save(function(err) {
    if (err) return res.status(400).send(err);
    else res.jsonp(policy);
  })
};

exports.updatePolicy = function(req, res) {
  Policy.findOneAndUpdate({'_id': req.params.policyId}, {$set: req.body}, {new: true}, function(err, updatedPolicy) {
    if (err) return res.status(400).send(err);
    else res.jsonp(updatedPolicy);
  });
};


// Headings

exports.getHeadings = function(req, res) {
   Heading.find({}).exec(function(err, heading) {
    if (err) return res.status(400).send(err);
    else res.jsonp(heading);
  });
};

exports.getHeading = function(req, res) {
  Heading.findOne({'type': req.params.headingName}).exec(function(err, heading) {
    if (err) return res.status(400).send(err);
    else res.jsonp(heading);
  });
};

exports.setHeading = function(req, res) {
  var heading = new Heading(req.body);
  heading.save(function(err) {
    if (err) return res.status(400).send(err);
    else res.jsonp(heading);
  })
};

exports.updateHeading = function(req, res) {
  Heading.findOneAndUpdate({'_id': req.params.headingId}, {$set: req.body}, {new: true}, function(err, updatedHeading) {
    if (err) return res.status(400).send(err);
    else res.jsonp(updatedHeading);
  });
};


//propertytype
exports.getPropertytype = function(req, res) {

  // Configure the request
  var options = {
    url: config.url + config.propertytypeApi,
    method: 'GET',
    headers: headers
  };

  // Start the request
  request(options, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      res.json(JSON.parse(body));
    }
  })
};

var fs = require('fs');
var Project = config.project;
var css = "views/" +Project+ "/modules/core/client/css/custom.css";

exports.getCss = function (req, res) {
  var cssData = fs.readFileSync(css, 'utf8');
  res.jsonp(cssData);
};

exports.updateCss = function (req, res) {
  fs.writeFile(css, req.body.css, 'utf8', function (err) {
    if (err) throw err;
    res.json({updated: true, file: req.body.css});
  })
};



