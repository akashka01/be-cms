'use strict';

/**
 * Module dependencies
 */
var cmsPolicy = require('../policies/cms.server.policy'),
  cms = require('../controllers/cms.server.controller');

module.exports = function (app) {


  app.route('/api/getPropertytype')
    .get(cms.getPropertytype);

  //get collections
  app.route('/api/selectedCollections')
    .get(cms.getSelectedCollections);
  app.route('/api/selectedCollection/:collectionId')
    .put(cms.updateSelectedCollection);
  app.route('/api/selectedCollection')
    .post(cms.setSelectedCollection);


  //get deals

  app.route('/api/deals:dealId')
    .get(cms.getMyDeals)
    .put(cms.updateMyDeal)
    .post(cms.setMyDeals);

  //get locations
  app.route('/api/selectedLocations')
    .get(cms.getSelectedLocations);
  app.route('/api/selectedLocation/:locationId')
    .put(cms.updateSelectedLocation);
  app.route('/api/selectedLocation')
    .post(cms.setSelectedLocation);

  //blogs
  app.route('/api/selectedBlogs')
    .get(cms.getSelectedBlogs);
  app.route('/api/selectedBlog/:blogId')
    .put(cms.updateSelectedBlogs);
  app.route('/api/selectedBlog')
    .post(cms.setSelectedBlog);


  //getpromos
  app.route('/api/getPromos')
    .get(cms.getPromos);
  app.route('/api/selectedPromos')
    .get(cms.getSelectedPromos);
  app.route('/api/selectedPromo/:promoId')
    .put(cms.updateSelectedPromo);
  app.route('/api/selectedPromo')
    .post(cms.setSelectedPromo);


  //get policy

  app.route('/api/policy')
    .get(cms.getPolicy)
    .post(cms.setPolicy);
  app.route('/api/policy/:policyId')
    .put(cms.updatePolicy)

  //headings
  app.route('/api/headings')
    .get(cms.getHeadings);
  app.route('/api/heading/:headingName')
    .get(cms.getHeading);
  app.route('/api/heading/:headingId')
    .put(cms.updateHeading);
  app.route('/api/heading')
    .post(cms.setHeading);


  app.route('/api/css')
    .get(cms.getCss);

};
