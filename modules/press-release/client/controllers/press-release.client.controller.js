(function() {
    'use strict';

    angular
        .module('pressRelease')
        .controller('pressReleaseController', pressReleaseController);

    pressReleaseController.$inject = ['$scope', 'Authentication', '$http', 'vcRecaptchaService', 'patterns','MetaTagsServices'];

    function pressReleaseController($scope, Authentication, $http, vcRecaptchaService, patterns, MetaTagsServices) {
      //to call meta tags for this page
      MetaTagsServices.metaTagHeader();

    }
}());
