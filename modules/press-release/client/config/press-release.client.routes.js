(function() {
    'use strict';

    angular
        .module('pressRelease.routes')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider', 'CLIENT'];

    function routeConfig($stateProvider, CLIENT) {
        $stateProvider
            .state('pressRelease', {
                url: '/press-release',
                templateUrl: '/views/' + CLIENT.name + '/modules/press-release/client/views/press-release.client.view.html',
                controller: 'pressReleaseController',
                data: {
                  meta: {
                    'title': CLIENT.capitalizeName + ' | Press Release',
                    'description': 'This is designed to appeal to bots',
                    'keywords': ''
                  }
                }
            });
    }
}());
