(function() {
    'use strict';

    angular
        .module('becomePartner')
        .controller('becomePartnerController', becomePartnerController);

    becomePartnerController.$inject = ['$scope', 'Authentication', '$http', 'vcRecaptchaService', 'patterns','MetaTagsServices'];

    function becomePartnerController($scope, Authentication, $http, vcRecaptchaService, patterns, MetaTagsServices) {
      //to call meta tags for this page
      MetaTagsServices.metaTagHeader();

    }
}());
