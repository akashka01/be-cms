'use strict';

var validator = require('validator'),
  path = require('path'),
  moment = require('moment'),
  mongoose = require('mongoose'),
  request = require('request'),
  _ = require('lodash'),
  config = require(path.resolve('./config/config')),
  projectConfig = require(path.resolve('./modules/core/server/config/config')),
  Hotel = mongoose.model('Hotels');

var configuration = projectConfig();

var headers = {
  apiKey: configuration.apiKey,
  channelId: configuration.channelId
};


/**
 * Render the main application page
 */

exports.socialCrawler = function(req,res,next) {
  var ua = req.headers['user-agent'];
  if (/^(facebookexternalhit)|(Twitterbot)|(Pinterest)/gi.test(ua)) {
    res.render('modules/core/server/views/social-index', {
      sharedConfig: JSON.stringify(config.shared),
      project: configuration.project,
      metaTags: req.metaTags
    });
  } else {
    next();
  }
};


//Static pages
exports.homePage = function(req,res,next) {

  var metaTags = {
    img       : 'http://' + req.headers.host + '/views/'+ configuration.project+ '/modules/core/client/images/slides/1_medium.jpg',
    url       : 'http://www.staydilly.com/',
    title     : 'Hotel Booking at Unbeatable Prices - Staydilly.com',
    descriptionText
      : 'Luxury hotels & resorts booking at the lowest prices. Great deals, up to 60% less, on a wide selection of popular hotels across South East Asia. 5-star hotels at 3-star prices!',
    imageUrl  : 'https://staydilly.com/'+ 'placeholder.png',
    keywords : "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
  };

  req.metaTags = metaTags;
  next();
};
exports.aboutUs = function(req,res,next) {

  var metaTags = {
    img       :  'http://' +req.headers.host + '/views/'+ configuration.project+ '/modules/core/client/img/header-pool-b.jpg',
    url       : 'http://www.staydilly.com/',
    title     : 'About Us - Staydilly.com',
    descriptionText
      : 'Revolutionize the travel industry. That is what we aim to do. How?',
    imageUrl  : 'http://www.staydilly.com/'+ 'placeholder.png',
    keywords : "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
  };
  req.metaTags = metaTags;
  next();
};
exports.FAQ = function(req,res,next) {

  var metaTags = {
    img       :  'http://' + req.headers.host + '/views/'+ configuration.project+ '/modules/core/client/img/header-thailand-staydilly-sunset-b.jpg',
    url       : 'http://www.staydilly.com/',
    title     : 'FAQs - Staydilly.com',
    descriptionText
      : 'Find answers to the questions we get asked the most on Staydilly',
    imageUrl  : 'http://www.staydilly.com/'+ 'placeholder.png',
    keywords : "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
  };
  req.metaTags = metaTags;
  next();
};

exports.contactUs = function(req,res,next) {

  var metaTags = {
    img       : 'http://' + req.headers.host + '/views/'+ configuration.project+ '/modules/core/client/img/header-kl-skyline-b.jpg',
    url       : 'http://www.staydilly.com/',
    title     : 'Contact Us - Staydilly.com',
    descriptionText
      : 'LiveChat, telephone, email or snail mail - les us know what Staydilly can do for you.',
    imageUrl  : 'http://www.staydilly.com/'+ 'placeholder.png',
    keywords : "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
  };
  req.metaTags = metaTags;
  next();
};
exports.careers = function(req,res,next) {

  var metaTags = {
    img       :  'http://' + req.headers.host + '/views/'+ configuration.project+ '/modules/core/client/images/slides/3.jpg',
    url       : 'http://www.staydilly.com/',
    title     : 'We are Hiring! - Staydilly.com',
    descriptionText
      : 'Are you ready for an adventure? Join our team today!',
    imageUrl  : 'http://www.staydilly.com/'+ 'placeholder.png',
    keywords : "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
  };
  req.metaTags = metaTags;
  next();
};
exports.pressRelease = function(req,res,next) {

  var metaTags = {
    img       : 'http://' + req.headers.host + '/views/'+ configuration.project+ '/modules/core/client/images/slides/3.jpg',
    url       : 'http://www.staydilly.com/',
    title     : 'Staydilly.com: Press Release - Staydilly.com',
    descriptionText: 'Press releases on Staydilly.com',
    imageUrl  : 'http://www.staydilly.com/'+ 'placeholder.png',
    keywords : "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
  };
  req.metaTags = metaTags;
  next();
};
exports.pressKit = function(req,res,next) {

  var metaTags = {
    img       : 'http://' + req.headers.host + '/views/'+ configuration.project+ '/modules/core/client/images/slides/3.jpg',
    url       : 'http://www.staydilly.com/',
    title     : 'Staydilly.com: Press Kit',
    descriptionText
      : 'Staydilly.com Press Kit',
    imageUrl  : 'http://www.staydilly.com/'+ 'placeholder.png',
    keywords : "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
  };
  req.metaTags = metaTags;
  next();
};
exports.partnersPortal = function(req,res,next) {

  var metaTags = {
    img       : 'http://' + req.headers.host + '/views/'+ configuration.project+ '/modules/core/client/images/slides/3.jpg',
    url       : 'http://www.staydilly.com/',
    title     : 'Staydilly.com: Partners Portal',
    descriptionText
      : 'This is designed to appeal to bots',
    imageUrl  : 'http://www.staydilly.com/'+ 'placeholder.png',
    keywords : "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
  };
  req.metaTags = metaTags;
  next();
};
exports.privacyPolicy = function(req,res,next) {

  var metaTags = {
    img       : 'http://' + req.headers.host + '/views/'+ configuration.project+ '/modules/core/client/images/slides/3.jpg',
    url       : 'http://www.staydilly.com/',
    title     : 'Staydilly.com: Privacy Policy',
    descriptionText
      : 'Siestaz Pte Ltd. Please read carefully these privacy policy and as they contain important information regarding your legal rights',
    imageUrl  : 'http://www.staydilly.com/'+ 'placeholder.png',
    keywords : "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
  };
  req.metaTags = metaTags;
  next();
};
exports.becomeAPartner = function(req,res,next) {

  var metaTags = {
    img       : 'http://' + req.headers.host + '/views/'+ configuration.project+ '/modules/core/client/images/slides/3.jpg',
    url       : 'http://www.staydilly.com/',
    title     : 'Staydilly.com: Become A Partner',
    descriptionText
      : 'This is designed to appeal to bots',
    imageUrl  : 'http://www.staydilly.com/'+ 'placeholder.png',
    keywords : "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
  };
  req.metaTags = metaTags;
  next();
};
exports.howWeWork = function(req,res,next) {

  var metaTags = {
    img       : 'http://' + req.headers.host + '/views/'+ configuration.project+ '/modules/core/client/img/header-beach-view-b.jpg',
    url       : 'http://www.staydilly.com/',
    title     : 'Staydilly.com: How We Work',
    descriptionText
      : 'Even the best hotels and most luxurious hotels have unsold rooms. Find out how we are able to offer you 5-star hotels at 3-star hotel prices. ',
    imageUrl  : 'http://www.staydilly.com/'+ 'placeholder.png',
    keywords : "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
  };
  req.metaTags = metaTags;
  next();
};

exports.termsAndConditions = function(req,res,next) {

  var metaTags = {
    img       : 'http://' + req.headers.host + '/views/'+ configuration.project+ '/modules/core/client/images/slides/3.jpg',
    url       : 'http://www.staydilly.com/',
    title     : 'Staydilly.com: Terms and Conditions',
    descriptionText
      : 'Siestaz Pte Ltd. Please read carefully these terms and conditions and as they contain important information regarding your rights',
    imageUrl  : 'http://www.staydilly.com/'+ 'placeholder.png',
    keywords : "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
  };
  req.metaTags = metaTags;
  next();
};

exports.hotelsResorts = function(req,res,next) {

  var metaTags = {
    img       : 'http://' + req.headers.host + '/views/'+ configuration.project+ '/modules/core/client/images/slides/3.jpg',
    url       : 'http://www.staydilly.com/',
    title     : 'Staydilly.com: Hotels and Resorts',
    descriptionText
      : 'Hotels and Resorts',
    imageUrl  : 'http://www.staydilly.com/'+ 'placeholder.png',
    keywords : "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
  };
  req.metaTags = metaTags;
  next();
};

//Dynamic pages

exports.hotelsDetailsPage = function(req,res,next) {
  var params = req.query;
  var param = {
    location: params.location ? '&cityId=' + params.location : '',
    stateId: params.stateId ? '&stateId=' + params.stateId : '',
    country: params.country ? '&countryId=' + params.country : '',
    checkin: params.checkin ? '&checkIn=' + moment(new Date(params.checkin)).format('DD/MM/YYYY') : '&checkIn=' + moment().add(1, 'd').format('DD/MM/YYYY'),
    checkout: params.checkout ? '&checkOut=' + moment(new Date(params.checkout)).format('DD/MM/YYYY') : '&checkOut=' + moment().add(2, 'd').format('DD/MM/YYYY'),
    propertyType: params.propertyType ? '&propertyTypeId=' + params.propertyType : '',
    dealId: params.deal ? '&dealId=' + params.deal : '',
    promoId: params.promo ? '&promoId=' + params.promo : ''
  };


  // Configure the request
  var options = {
    url: configuration.url + configuration.searchApi + configuration.beId +
    param.location + param.stateId + param.country + param.checkin + param.checkout +
    param.propertyType + param.dealId + param.promoId,
    method: 'GET',
    headers: headers
  };


  if(params.location){
    // Start the request
    request(options, function(error, response, body) {
      if (!error && response.statusCode == 200) {
        var hotelList = JSON.parse(body);
        var selectedHotel = hotelList.Hotel_Details[0];

        var metaTags = {
          img       :  selectedHotel.images[0],
          url       : req.headers.host + '/' + req.url,
          title     : selectedHotel.address.city + ", " + selectedHotel.address.country + " Hotel Deals - Staydilly.com", 
          descriptionText : 'Hotels in ' + selectedHotel.address.city + ", " + selectedHotel.address.state + ", " + selectedHotel.address.country + ". Luxury hotels & resorts booking at the lowest prices. Great deals, up to 60% less, on a wide selection of popular hotels across South East Asia. 5-star hotels at 3-star prices!" ,
          imageUrl  : selectedHotel.images[0],
          keywords : "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
        };
        req.metaTags = metaTags;
      }
      next();
    });
  }else if(params.stateId){
    // Start the request
    request(options, function(error, response, body) {
      if (!error && response.statusCode == 200) {
        var hotelList = JSON.parse(body);
        var selectedHotel = hotelList.Hotel_Details[0];

        var metaTags = {
          img: selectedHotel.images[0],
          url: req.headers.host + '/' + req.url,
          title: selectedHotel.address.state  + ", " + selectedHotel.address.country + " Hotel Deals - Staydilly.com",
          descriptionText: 'Hotels in ' + selectedHotel.address.city + ", " + selectedHotel.address.state + ", " + selectedHotel.address.country + ". Luxury hotels & resorts booking at the lowest prices. Great deals, up to 60% less, on a wide selection of popular hotels across South East Asia. 5-star hotels at 3-star prices!" ,
          imageUrl: selectedHotel.images[0],
          keywords : "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
        };
        req.metaTags = metaTags;
        next();
      }
    });
  }else if(params.country){
    // Start the request
    request(options, function(error, response, body) {
      if (!error && response.statusCode == 200) {
        var hotelList = JSON.parse(body);
        var selectedHotel = hotelList.Hotel_Details[0];

        var metaTags = {
          img: selectedHotel.images[0],
          url: req.headers.host + '/' + req.url,
          title: selectedHotel.address.country + " Hotel Deals - Staydilly.com",
          descriptionText: 'Hotels in ' + selectedHotel.address.country + ". Luxury hotels & resorts booking at the lowest prices. Great deals, up to 60% less, on a wide selection of popular hotels across South East Asia. 5-star hotels at 3-star prices!" ,
          imageUrl: selectedHotel.images[0],
          keywords : "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
        };
        req.metaTags = metaTags;
        next();
      }
    });
  }else {
    // Start the request
    request(options, function(error, response, body) {
      if (!error && response.statusCode == 200) {
        var hotelList = JSON.parse(body);
        var selectedHotel = hotelList.Hotel_Details[0];

        var metaTags = {
          img: selectedHotel.images[0],
          url: req.headers.host + '/' + req.url,
          title: 'Hotel Booking and Deals at Unbeatable Prices  - Staydilly.com',
          descriptionText: 'Luxury hotels & resorts booking at the lowest prices. Great deals, up to 60% less, on a wide selection of popular hotels across South East Asia. 5-star hotels at 3-star prices',
          imageUrl: selectedHotel.images[0],
          keywords : "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
        };
        req.metaTags = metaTags;
        next();
      }
    });
  }
};


exports.listHotelsDetailsPage = function(req,res,next) {
  var selectedParams = req.params;
  var cityId,stateId,countryId;

  // Configure the request
  var options = {
    url: configuration.url + configuration.locationApi,
    method: 'GET',
    headers: headers
  };

  // Start the request
  request(options, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      var locations = JSON.parse(body);
        locations = locations.locations;
      if (selectedParams.cityName) {
        selectedParams.cityName = (selectedParams.cityName).replace(/\-/g, ' ');
        var city = _.find(locations, { 'city': selectedParams.cityName });
         cityId = city.city_id;
      } else if (selectedParams.stateName) {
        selectedParams.stateName = (selectedParams.stateName).replace(/\-/g, ' ');
        var state = _.find(locations, { 'state': selectedParams.stateName });
         stateId = state.state_id;
      } else if (selectedParams.countryName) {
        selectedParams.countryName = (selectedParams.countryName).replace(/\-/g, ' ');
        var country = _.find(locations, { 'country': selectedParams.countryName });
         countryId = country.country_id;
      }
    }
    var param = {
      cityId: cityId ? '&cityId=' + cityId : '',
      stateId: stateId ? '&stateId=' + stateId : '',
      countryId: countryId ? '&countryId=' + countryId : ''
    };

    // Configure the request
    var hotelsOptions = {
      url: configuration.url + configuration.searchApi + configuration.beId +
      param.cityId + param.stateId + param.countryId,
      method: 'GET',
      headers: headers
    };
    if(selectedParams.cityName){
      // Start the request
      request(hotelsOptions, function(error, response, body) {
        if (!error && response.statusCode == 200) {
          var hotelList = JSON.parse(body);
          var selectedHotel = hotelList.Hotel_Details[0];

          var metaTags = {
            img       :  selectedHotel.images[0],
            url       : req.headers.host + '/' + req.url,
            title     : selectedHotel.address.city,
            descriptionText : 'Hotels in ' + selectedHotel.address.city ,
            imageUrl  : selectedHotel.images[0],
            keywords : "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
          };
          req.metaTags = metaTags;
        }
        next();
      });
    }else if(selectedParams.stateName){
      // Start the request
      request(hotelsOptions, function(error, response, body) {
        if (!error && response.statusCode == 200) {
          var hotelList = JSON.parse(body);
          var selectedHotel = hotelList.Hotel_Details[0];

          var metaTags = {
            img: selectedHotel.images[0],
            url: req.headers.host + '/' + req.url,
            title: selectedHotel.address.state,
            descriptionText: 'Hotels in ' + selectedHotel.address.state,
            imageUrl: selectedHotel.images[0],
            keywords: "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
          };
          req.metaTags = metaTags;
          next();
        }
      });
    }else if(selectedParams.countryName){
      // Start the request
      request(hotelsOptions, function(error, response, body) {
        if (!error && response.statusCode == 200) {
          var hotelList = JSON.parse(body);
          var selectedHotel = hotelList.Hotel_Details[0];

          var metaTags = {
            img: selectedHotel.images[0],
            url: req.headers.host + '/' + req.url,
            title: selectedHotel.address.country,
            descriptionText: 'Hotels in ' + selectedHotel.address.country,
            imageUrl: selectedHotel.images[0],
            keywords: "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
          };
          req.metaTags = metaTags;
          next();
        }
      });
    }
  });
};

exports.hotelDetailPage = function(req,res,next) {

  var params = req.query;
  var hotelId = req.params.selectedHotelId;
  var selectedHotel = {};

  var param = {
    searchId: '&searchId=' + params.searchId,
    productId: '&productId=' + params.productId,
    checkIn: params.checkin ? '&checkIn=' + moment(new Date(params.checkin)).format('DD/MM/YYYY') : '&checkIn=' + moment().add(1, 'd').format('DD/MM/YYYY'),
    checkOut: params.checkout ? '&checkOut=' + moment(new Date(params.checkout)).format('DD/MM/YYYY') : '&checkOut=' + moment().add(2, 'd').format('DD/MM/YYYY')
  };


  // Configure the request
  var options = {
    url: configuration.url + configuration.roomApi + configuration.beId + param.searchId + param.productId + param.checkIn + param.checkOut,
    method: 'GET',
    headers: headers
  };

  // Start the request
  request(options, function(error, response, body) {
    if (!error && response.statusCode == 200) {
        var hotelList = JSON.parse(body);
        selectedHotel = hotelList.Hotel_Details[0];
        var metaTags = {
          img       : selectedHotel.images[0],
          url       : req.headers.host + '/' + req.url,
          title     : selectedHotel.hotel_name + " Hotel in " + selectedHotel.address.state + ", " + selectedHotel.address.country + " - Staydilly.com",
          descriptionText : selectedHotel.description,
          imageUrl  : selectedHotel.images[0],
          keywords : "Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation"
        };
        req.metaTags = metaTags;
        next();
    }
  });


};

