(function() {
        'use strict';

        angular
            .module('core')
            .controller('footerController', footerController);

  footerController.$inject = ['$scope', '$state', 'Authentication', '$stateParams', '$rootScope'
        ];

        function footerController($scope, $state, Authentication, $stateParams, $rootScope) {

          var maxDate = new Date(2020, 5, 22);

          $scope.clear = function() {
            $scope.startDate = null;
          };

          var n = 1; //number of days to add (cutoff days).
          var today = new Date();
          var sd = new Date(today.getFullYear(), today.getMonth(), today.getDate() + n);
          var ed = new Date(today.getFullYear(), today.getMonth(), today.getDate() + (n + 1));

          $rootScope.startDate = $stateParams.checkin ? new Date(strToDate($stateParams.checkin)) : sd;
          $rootScope.endDate = $stateParams.checkout ? new Date(strToDate($stateParams.checkout)) : ed;
          $rootScope.guest = {
            rooms: $stateParams.rooms ? $stateParams.rooms : 1,
            adults: $stateParams.adults ? $stateParams.adults : 2
          };


        }
    }());
