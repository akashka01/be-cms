(function() {
  'use strict';

  angular
    .module('core')
    .controller('MainController', MainController);

  MainController.$inject = ['$scope', 'Authentication', '$rootScope', '$http', '$state', 'ngMeta', 'CLIENT','$timeout'];

  function MainController($scope, Authentication, $rootScope, $http, $state, ngMeta, CLIENT,$timeout) {
    //change the footer and header for cms
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
      var isInCmsState = $state.includes("cms.**");
      if(isInCmsState){
        $rootScope.cmsLayout = true;
      }else{
        $rootScope.cmsLayout = false;
      }
    });
    $rootScope.hotelsMetaTags = function() {
      if ($rootScope.locationcity) {
        ngMeta.setTitle($rootScope.locationcity + ", " + $rootScope.locationcountry + " Hotel Deals" + ' | ' + CLIENT.capitalizeName);
        ngMeta.setTag('description', 'Hotel deals in ' + $rootScope.locationcity + ", " + $rootScope.locationstate +"." + " Book your discounted hotels in " + $rootScope.locationstate + " today.");
        ngMeta.setTag('keywords', 'Discounted hotels in ' + $rootScope.locationcity + ", " + $rootScope.locationstate +"." + " Lodging, accommodation, discount hotel, online booking, online reservation, hotels, special offer, specials, weekend break, deals, budget, cheap, savings");
      } else if ($rootScope.locationstate) {
        ngMeta.setTitle($rootScope.locationstate  + ", " + $rootScope.locationstatecountry + " Hotel Deals" + ' | ' + CLIENT.capitalizeName);
        ngMeta.setTag('keywords', 'Discounted hotels in ' + $rootScope.locationstate +"." + " Lodging, accommodation, discount hotel, online booking, online reservation, hotels, special offer, specials, weekend break, deals, budget, cheap, savings" );
        ngMeta.setTag('description', 'Hotel deals in ' + $rootScope.locationstate +"." + " Book your discounted hotels in " + $rootScope.locationstate + " today.");
      } else if ($rootScope.locationCountry) {
        ngMeta.setTitle($rootScope.locationCountry + " Hotel Deals" + ' | ' + CLIENT.capitalizeName);
        ngMeta.setTag('description', 'Hotels deals in ' + $rootScope.locationcountry  +"." + " Book your discounted hotels in " + $rootScope.locationstate + " today.");
        ngMeta.setTag('keywords', 'Discounted Hotels in ' + $rootScope.locationcountry +"." + " Lodging, accommodation, discount hotel, online booking, online reservation, hotels, special offer, specials, weekend break, deals, budget, cheap, savings");
      }
    };
  }
}());
