(function() {
        'use strict';

        angular
            .module('core')
            .controller('HeaderController', HeaderController);

        HeaderController.$inject = ['$scope', '$state', 'Authentication', 'menuService', '$uibModal', '$log', '$document', '$rootScope', '$stateParams', '$http',
            '$cookieStore', '$cookies', 'CLIENT'
        ];

        function HeaderController($scope, $state, Authentication, menuService, $uibModal, $log, $document, $rootScope, $stateParams, $http,
            $cookieStore, $cookies, CLIENT) {
            $scope.bannerMessage = '';
            var currencyMultipyNUmber = 1.03;
            var $ctrl = this;
            $scope.header = {

                "logo": {
                    "alt": "header logo",
                    "src": "/views/staydilly/modules/core/client/images/logo.png"
                },
                "topnavlink": [{
                        "title": "How we work",
                        "url": "howWeWork"
                    },
                    {
                        "title": "Deals",
                        "url": "deals"
                    }
                    // {
                    //     "title": "Destinations",
                    //     "url": "destination"
                    // }
                    // {
                    // 	"title" : "About",
                    // 	"url" : "about"
                    // }
                ]

            };
           $scope.checkin = moment().add(1, 'd').format('MM-DD-YYYY');
            $scope.checkout = moment().add(2, 'd').format('MM-DD-YYYY');

          // login widget popUp

          var $ctrl = this;

          $ctrl.animationsEnabled = true;

          // Currency Converter
          var currencyConverter = function(newCurrency){
              $rootScope.currencyMultiplier = {};
              $rootScope.currencyFlag = false;
              var j = 0;
              for(var i = 0; i < $scope.uniqueCurrency.length; i++){
              $http.get('http://api.fixer.io/latest?base='+$scope.uniqueCurrency[i]+'&symbols='+newCurrency).success(function(res){
                $rootScope.currencyMultiplier[res.base] = (res.rates[newCurrency] != undefined)? res.rates[newCurrency]*$scope.multiplier : 1;
                j++;
                if(j === $scope.uniqueCurrency.length){
                  $rootScope.currency = newCurrency;
                  $rootScope.currencyFlag = true;
                }
              });
            }
          };
          $scope.multiplier = (CLIENT.name == 'staydilly')? currencyMultipyNUmber : 1;
          $scope.uniqueCurrency = (CLIENT.name == 'staydilly') ? ["THB","MYR", "IDR", "USD"] : undefined;
          $rootScope.shiftDecimal = {"THB": 2, "MYR": 2, "IDR": 0, "USD": 2, "AUD": 2, "EUR": 2, "SGD": 2, "THB": 2, "MVR": 2};
          
          $scope.onCurrencyChange = function(newCurrency){
              $scope.currency = newCurrency;
            $cookieStore.put('Currency', newCurrency);
              if($scope.uniqueCurrency != undefined && $scope.uniqueCurrency.length > 0){
                currencyConverter(newCurrency);
              }
              else{
                $http.get('/api/getHotels').success(function(res) {
                    $scope.uniqueCurrency = _.uniq(_.map(_.flatten(res.Hotel_Details), 'currency'));
                    currencyConverter(newCurrency);
                });
              }
          };

          $scope.currency = "Currency";
          var nCurrency = $cookieStore.get('Currency');
          if(nCurrency!= undefined){$scope.onCurrencyChange(nCurrency);}
        }
    }());
