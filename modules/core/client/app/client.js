(function (app) {
  'use strict';

  app.registerModule('config');

  angular
    .module('config')
    .constant('CLIENT', {
      'name': 'staydilly',
      'capitalizeName': 'Staydilly'
    })
  .run(['ngMeta', function(ngMeta) {
    ngMeta.init();
  }]);

}(ApplicationConfiguration));
