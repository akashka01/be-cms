(function() {
    'use strict';

    angular
        .module('termsConditions')
        .controller('termsConditionsController', termsConditionsController);

    termsConditionsController.$inject = ['$scope', 'Authentication', '$http', 'vcRecaptchaService', 'patterns','MetaTagsServices'];

    function termsConditionsController($scope, Authentication, $http, vcRecaptchaService, patterns, MetaTagsServices) {
      //to call meta tags for this page
      MetaTagsServices.metaTagHeader();

    }
}());
