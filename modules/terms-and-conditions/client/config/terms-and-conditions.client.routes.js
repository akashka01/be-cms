(function() {
    'use strict';

    angular
        .module('termsConditions.routes')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider', 'CLIENT'];

    function routeConfig($stateProvider, CLIENT) {
        $stateProvider
            .state('termsConditions', {
                url: '/terms-and-conditions',
                templateUrl: '/views/' + CLIENT.name + '/modules/terms-and-conditions/client/views/terms-and-conditions.client.view.html',
                controller: 'termsConditionsController',
                data: {
                  meta: {
                    'title': CLIENT.capitalizeName + ' | Terms and Conditions',
                    'description': 'Siestaz Pte Ltd. Please read carefully these terms and conditions and as they contain important information regarding your rights',
                    'keywords': ''
                  }
                }
            });
    }
}());
