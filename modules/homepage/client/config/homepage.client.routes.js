(function () {
  'use strict';

  angular
    .module('homepage.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider', 'CLIENT'];

  function routeConfig($stateProvider, CLIENT) {
    $stateProvider
      .state('homepage', {
        url: '/',
        templateUrl: '/views/' + CLIENT.name + '/modules/homepage/client/views/homepage.client.view.html',
        controller: 'HomePageController',
        controllerAs: 'vm',
        data: {
          meta: {
            'title': CLIENT.capitalizeName + ' | Hotel Booking at Unbeatable Prices',
            'description': 'Luxury hotels & resorts booking at the lowest prices. Great deals, up to 60% less, on a wide selection of popular hotels across South East Asia. 5-star hotels at 3-star prices!',
            'keywords' : 'Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation'
          }
        }
      })
      .state('index', {
        url: '/index.html',
        templateUrl: '/views/' + CLIENT.name + '/modules/homepage/client/views/homepage.client.view.html',
        controller: 'HomePageController',
        controllerAs: 'vm',
        data: {
          meta: {
            'title': CLIENT.capitalizeName + ' | Hotel Booking at Unbeatable Prices',
            'description': 'Luxury hotels & resorts booking at the lowest prices. Great deals, up to 60% less, on a wide selection of popular hotels across South East Asia. 5-star hotels at 3-star prices!',
            'keywords' : 'Staydilly, hotel, hotels, booking, discount, reservations, deals, cheap, travel, Asia, South East Asia, Kuala Lumpur, Bangkok, Phuket, Thailand, Malaysia, Bali, Melaka, Penang, Chiangmai, Chiangmai, Pattaya, acommodation, accomodation, accommodation'
          }
        }
      });
  }
}());
