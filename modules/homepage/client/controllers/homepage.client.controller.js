(function() {
    'use strict';

    angular
        .module('homepage')
        .controller('HomePageController', HomePageController);

    HomePageController.$inject = ['$scope', 'Authentication', '$rootScope', '$http', 'Calculate', '$window', 'CLIENT', 'HotelsServices', '$timeout'];

    function HomePageController($scope, Authentication, $rootScope, $http, Calculate, $window, CLIENT, HotelsServices, $timeout) {
      var netExclusive, markup, sellExclusive, discountedSellExclusive, finalPrice, finalTaxes, discount, Baor_rate, fix_rate, Show_discount;
      $scope.widgetText = 'Book a room';
      $rootScope.dataReceived = true;

      $scope.promos = [{
        id: 6551,
        showWidget: false,
        default_url: '1_default.jpg',
        portrait_url: '1_portrait.jpg',
        small_url: '1_small.jpg',
        medium_url: '1_medium.jpg',
        large_url: '1_large.jpg',
        book: 'Book Now',
        from: 'from',
        title: 'Explore Asia',
        pricelarge: 140,
        currency: 'MYR',
        desc: 'Getaway this Christmas by exploring a new city with Staydilly with that special someone. For that truly local experience, stay at one of Staydilly Local Charm hotels.'
      }, {
        id: 6550,
        showWidget: false,
        default_url: '2_default.jpg',
        portrait_url: '2_portrait.jpg',
        small_url: '2_small.jpg',
        medium_url: '2_medium.jpg',
        large_url: '2_large.jpg',
        book: 'Book Now',
        from: 'from',
        title: 'Stay at Staydilly',
        pricelarge: 560000,
        currency: 'IDR',
        desc: 'Who is up for some fun and adventure? A romantic getaway perhaps?  At Staydilly, we have a wide range of hotels to suit whatever your preference may be across Southeast Asia.'
      }, {
        id: 6549,
        showWidget: false,
        default_url: '3_default.jpg',
        portrait_url: '3_portrait.jpg',
        small_url: '3_small.jpg',
        medium_url: '3_medium.jpg',
        large_url: '3_large.jpg',
        book: 'Book Now',
        from: 'from',
        title: 'Family Fun',
        pricelarge: 751,
        currency: 'THB',
        desc: 'Spend quality time with the family this year-end at the beach. Select a hotel from our Beach Resorts for that family getaway!'
      }];

      $http.get('/api/getPromos').success(function(res) {
        var promos = res.promos;
        _($scope.promos).forEach(function(val) {
          var promo = _.find(promos, { 'id': val.id });
          if (promo) {
            val.discount = getPercentage(promo);
            val.description = val.desc ? val.desc : promo.description;

            // val.pricelarge = promo.price;
            // val.currency = promo.currency;
          }
        });
      });

      $http.get('/api/getDeals').success(function(res) {
        var deals = res.dealsList;
        _($scope.deals).forEach(function(val) {
          var deal = _.find(deals, { 'id': val.id });
          if (deal) {
            val.currency = deal.currency;
            val.discount = getPercentage(deal);
            val.oldPrice = deal.price;
            var discount = Math.round((val.discount / 100) * val.oldPrice);
            val.price = val.oldPrice - discount;
          }
        });

      });

      function getPercentage(deal) {
        if (deal.discount_type == 'Percentage') {
          return deal.value;
        } else {
          return Math.round((deal.value * 100) / deal.price);
        }
      }

      $scope.deals = [{
        id: 71500,
        image: '/views/staydilly/modules/core/client/images/deals/1.jpg'
      }, {
        id: 71547,
        image: '/views/staydilly/modules/core/client/images/deals/2.jpg'
      }, {
        id: 71519,
        image: '/views/staydilly/modules/core/client/images/deals/3.jpg'
      }, {

        id: 71574,
        image: '/views/staydilly/modules/core/client/images/deals/staydilly-phuket-naithon-beach.jpg'

      }, {
        id: 71530,
        image: '/views/staydilly/modules/core/client/images/deals/5.jpg'
      }, {

        id: 72742,
        large: true,
        image: '/views/staydilly/modules/core/client/images/deals/staydilly-village-resort-krabi-thailand.jpg'
      }];
      $scope.largeDeal = _.find($scope.deals, 'large');

      function calculateAll(hotel) {
        hotel.sellExclusive = 0;
        hotel.discountedSellExclusive = 0;
        hotel.Baor_rate = 0;

        var taxes = hotel.taxes;
        var multipleRates = hotel.multipleRates[0];

        var prices = {
          netInclusive: multipleRates.netRate,
          serviceTax: _.find(taxes, { 'id': 1 }) ? (_.find(taxes, { 'id': 1 }).value / 100) : 0,
          cityTax: _.find(taxes, { 'id': 3 }) ? (_.find(taxes, { 'id': 3 }).value / 100) : 0,
          gst: _.find(taxes, { 'id': 2 }) ? (_.find(taxes, { 'id': 2 }).value / 100) : 0,
          other: _.find(taxes, { 'id': 4 }) ? (_.find(taxes, { 'id': 4 }).value / 100) : 0,
          markup: (multipleRates.markupRate / 100),
          discount: (hotel.deal.value / 100)

        };

        netExclusive = Calculate.getNetExclusive(prices.netInclusive, prices.serviceTax, prices.cityTax, prices.other, prices.gst);
        markup = Calculate.getMarkupValue(prices.netInclusive, prices.markup);
        sellExclusive = Calculate.getSellExclusive(netExclusive, markup);
        discountedSellExclusive = Calculate.getDiscountedSellExclusive(sellExclusive, prices.discount);
        finalPrice = Calculate.getFinalPrice(discountedSellExclusive, prices.serviceTax, prices.cityTax, prices.other, prices.gst);
        finalTaxes = Calculate.getFinalTaxes(finalPrice, discountedSellExclusive);
        discount = Calculate.getDiscount(sellExclusive, prices.discount);
        hotel.sellExclusive += Calculate.calculateAmount(Math.round(sellExclusive), $scope.guest);
        hotel.discountedSellExclusive += Calculate.calculateAmount(Math.round(discountedSellExclusive), $scope.guest);
        hotel.discounts = prices.discount;
        hotel.Rack_rate = multipleRates.rackRate;
        if (hotel.Baor_rate == '0') {
          hotel.Baor_rate = hotel.Rack_rate;
        }
        if (hotel.Baor_rate >= hotel.Rack_rate) {
          hotel.fix_rate = hotel.Rack_rate;
        } else {
          hotel.fix_rate = hotel.Baor_rate;
        }
        hotel.Show_discount = Calculate.getdiscountpercent(sellExclusive, hotel.fix_rate);
      }

    //   $http.get('/api/getHotels').success(function(res) {
      var dealsCalculation = function(n){
        var today = new Date();
        var params = {
          adults: "2", rooms:"1",
          checkin: new Date(today.getFullYear(), today.getMonth(), today.getDate() + n),
          checkout: new Date(today.getFullYear(), today.getMonth(), today.getDate() + (n+1))
        };

        HotelsServices.get(params).success(function(res) {
          $scope.searchid = res.search_id;
          _($scope.hoteldeals).forEach(function(val) {
              if(!val.isStock){
                var hoteldel = _.find(res.Hotel_Details, { 'hotel_id': val.id });
                if (hoteldel) {
                  calculateAll(hoteldel);
                  val.address = hoteldel.address.city;
                  val.hotel_name = hoteldel.hotel_name;
                  val.currency = hoteldel.currency;
                  val.sellExclusive = hoteldel.sellExclusive;
                  val.discount = hoteldel.Show_discount;
                  val.startDate = params.checkin;
                  val.endDate = params.checkout;
                  val.isStock = true;
                  hoteldeal.push(val);
                  if(val.large) ($scope.large_hotel = val);
                }
              }
          });
          if($scope.hoteldeals.length != hoteldeal.length){
            n=n+5;
            dealsCalculation(n);
          }
          else{
            $scope.hoteldeal = angular.copy(hoteldeal);
          }
        });
      }

      var hoteldeal = [];
      $scope.hoteldeals = [{
        id: 72546,
        image: '/views/staydilly/modules/core/client/images/deals/72546.jpg',
        isStock: false
      }, {
        id: 71514,
        isStock: false,
        image: '/views/staydilly/modules/core/client/images/deals/71514.jpg'
      }, {
        id: 71554,
        isStock: false,
        image: '/views/staydilly/modules/core/client/images/deals/71554.jpg'
      }, {
        isStock: false,
        id: 72959,
        image: '/views/staydilly/modules/core/client/images/deals/72959.jpg'
      }, {
        id: 73363,
        isStock: false,
        image: '/views/staydilly/modules/core/client/images/deals/73363.jpg'
      }, {
        isStock: false,
        id: 73444,
        large: true,
        image: '/views/staydilly/modules/core/client/images/deals/73444.jpg'
      }];
      $scope.hoteldeal = angular.copy($scope.hoteldeals);
      $scope.large_hotel = _.find($scope.hoteldeal, { 'large': true });
      dealsCalculation(1);

      $http.get('/api/getPropertytype').success(function(res) {
        var propertyTypes = res.PropertyList;

        _($scope.collections).forEach(function(val) {

          var col = _.find(propertyTypes, { 'id': val.id });

          if (col) {
            val.name = col.name;
          }

        });

      });

      $scope.collections = [{
        id: 7,
        small: true,
        image: "/views/staydilly/modules/core/client/images/collections/city-hotel-on-staydilly.jpg",
        description: 'Charming hotels in the heart of Asia¹s most vibrant cities'
      }, {
        id: 24,
        large: true,
        image: "/views/staydilly/modules/core/client/images/collections/luxury-hotels-on-staydilly.jpg",
        description: 'Pamper yourself in some of the best hotels in the world'
      }, {
        id: 22,
        image: "/views/staydilly/modules/core/client/images/collections/boutique-hotel-on-staydilly.jpg",
        small: true,
        description: 'Quirky hotels for the adventurer in you'
      }, {
        id: 10,
        small: true,
        image: "/views/staydilly/modules/core/client/images/collections/beach-resort-on-staydilly.jpg",
        description: 'Beautiful hotels for your beach escapades'
      }, {
        id: 1,
        image: "/views/staydilly/modules/core/client/images/collections/business-hotel-on-staydilly.jpg",
        bottom: true,
        description: 'Hotels you will love to come home to after a long day in the office'
      }, {
        id: 25,
        image: "/views/staydilly/modules/core/client/images/collections/local-chic-hotel-staydilly.jpg",
        bottom: true,
        description: 'Excite your palette with a taste of the local flavour'
      }

      ];

      $scope.largecollections = _.find($scope.collections, 'large');
      $scope.smallcollections = _.filter($scope.collections, 'small');
      $scope.bottomcollections = _.filter($scope.collections, 'bottom');

      $http.get('/api/blogs').success(function(res) {
        $scope.blogs = [];
        for(var i = 0; i < 4; i++) $scope.blogs.push(res[i]);
      });

      $scope.showblogs = [5];


      $http.get('/api/getLocations').success(function(loc) {
        var states = loc;
        _($scope.states).forEach(function(val) {

          val.citiesInState = _.groupBy(loc, ['state_id', val.id]);

        });

        var dividedLocations = _.uniq(_.map(loc.locations,"state_id"));
          for (var i = 0; i < dividedLocations.length; i++){
              var dl = _.find(loc.locations, {'state_id': dividedLocations[i]});
              var newState = {
                  city: "",
                  city_id: -1,
                  country: dl.country,
                  country_id: dl.country_id,
                  lattitude: dl.lattitude,
                  longitude: dl.longitude,
                  state: dl.state,
                  state_id: dl.state_id,
              };
              loc.locations.unshift(newState);
          }

      });

      $scope.states = [{
        id: 831, //Chiangmai
        image: '/views/staydilly/modules/core/client/images/cities/chiangmai-staydilly-hotels.jpg'

      }, {
        id: 622, //KL
        image: '/views/staydilly/modules/core/client/images/cities/kuala-lumpur-staydilly-hotels.jpg'

      }, {
        id: 89,//bangkok
        image: '/views/staydilly/modules/core/client/images/cities/bangkok-staydilly-hotels.jpg'
      },
        {
          id: 500,//bali
          image: '/views/staydilly/modules/core/client/images/cities/bali-staydilly-hotels.jpg'
        }, {
          id: 926,//melaka
          image: '/views/staydilly/modules/core/client/images/cities/melaka-staydilly-hotels.jpg'
        },
        {
          id: 743,//phuket
          image: '/views/staydilly/modules/core/client/images/cities/phuket-staydilly-hotels.jpg',
          large: 'true'
        }
      ];
      $scope.largeState = _.find($scope.states, 'large');
      $scope.Travels = [

        {
          image: '/views/staydilly/modules/core/client/images/blog/1.png',
          header: '20 Must See and Do in Penang Island',
          description: 'Discover new hotels, unique boutique hotels and hidden gems, From top-line luxury to smart...'
        }, {
          image: '/views/staydilly/modules/core/client/images/blog/2.png',
          header: 'The Floating Markets in Bangkok',
          description: 'Discover new hotels, unique boutique hotels and hidden gems, From top-line luxury to smart...'
        }, {
          image: '/views/staydilly/modules/core/client/images/blog/3.png',
          header: '20 Must See and Do in Bali',
          description: 'Discover new hotels, unique boutique hotels and hidden gems, From top-line luxury to smart...'
        }, {
          image: '/views/staydilly/modules/core/client/images/blog/4.png',
          header: 'The Floating Markets in Bangkok',
          description: 'Discover new hotels, unique boutique hotels and hidden gems, From top-line luxury to smart...'
        }
      ];

      $scope.sections = {
        deals: {
          icon: '/views/staydilly/modules/core/client/images/home/deals.png',
          header: 'Amazing Travel Deals',
          description: 'Don’t want to skimp on luxury when you’re travelling? We don’t believe you have to! Staydilly offers the best hotel deals at unbeatable prices. Browse through our deals and book yourself a Staydilly getaway today!'
        },
        collections: {
          icon: '/views/staydilly/modules/core/client/images/home/collections.png',
          header: 'Hotels by Collection',
          description: 'Whether it’s a business, leisure or family getaway you’re in the mood for we have something for everyone. Browse our hotel collections and book yourself a Staydilly getaway today!'
        },
        cities: {
          icon: '/views/staydilly/modules/core/client/images/home/cities.png',
          header: 'Discover Cities to Visit',
          description: 'The bright lights are beckoning! Experience some of Asia’s beautiful cities at the height of luxury with Staydilly. Book yourself a Staydilly getaway today!'
        },
        blog: {
          icon: '/views/staydilly/modules/core/client/images/home/blog.png',
          header: 'Travel Tales',
          description: 'Read through these tales to kick-start your Staydilly getaway. With the best hotel deals in a variety of cities, you’ll be spoilt for choice. What will your story be? Book yourself a Staydilly getaway today!'
        }
      };

      $scope.sliderimages = [{
        url: "1.jpg",
        title: "promo 1",
        description: "Promo Description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        from: "from",
        pricelarge: "$29",
        pricesmall: ".90",
        book: "Book Now"
      }, {
        url: "2.jpg",
        title: "promo 2",
        description: "Promo Description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        from: "from",
        pricelarge: "$29",
        pricesmall: ".90",
        book: "Book Now"

      }, {
        url: "3.jpg",
        title: "promo 3",
        description: "Promo Description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        from: "from",
        pricelarge: "$29",
        pricesmall: ".90",
        book: "Book Now"

      }, {
        url: "4.jpg",
        title: "promo 4",
        description: "Promo Description Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        from: "from",
        pricelarge: "$29",
        pricesmall: ".90",
        book: "Book Now"

      }];

        document.addEventListener('touchstart', function() {}, true);

        // Carousel
        $scope.myInterval = 3000;
        $scope.noWrapSlides = false;
        $scope.active = 0;
    }
}());
