(function() {
    'use strict';

    angular
        .module('privacyPolicy')
        .controller('privacyPolicyController', privacyPolicyController);

    privacyPolicyController.$inject = ['$scope', 'Authentication', '$http', 'vcRecaptchaService', 'patterns','MetaTagsServices'];

    function privacyPolicyController($scope, Authentication, $http, vcRecaptchaService, patterns, MetaTagsServices) {
      //to call meta tags for this page
      MetaTagsServices.metaTagHeader();

    }
}());
