(function() {
    'use strict';

    angular
        .module('privacyPolicy.routes')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider', 'CLIENT'];

    function routeConfig($stateProvider, CLIENT) {
        $stateProvider
            .state('privacyPolicy', {
                url: '/privacy-policy',
                templateUrl: '/views/' + CLIENT.name + '/modules/privacy-policy/client/views/privacy-policy.client.view.html',
                controller: 'privacyPolicyController',
                data: {
                  meta: {
                    'title': CLIENT.capitalizeName + ' | Privacy Policy',
                    'description': 'Siestaz Pte Ltd. Please read carefully these privacy policy and as they contain important information regarding your legal rights',
                    'keywords': ''
                  }
                }
            });
    }
}());
