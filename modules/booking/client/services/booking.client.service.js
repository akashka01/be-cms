(function () {
  'use strict';

  angular
    .module('booking.services')
    .factory('BookingServices', BookingServices);

  BookingServices.$inject = ['$resource', '$log', '$http'];

  function BookingServices($resource, $log, $http) {
	return {
	  getOrderDetails: function(params) {
	    return $http.get('/api/getSingleBookingDetails', {
	      headers: {
	        "params": JSON.stringify(params)
	      }
	    });
	  }
	}
  }	
}());
