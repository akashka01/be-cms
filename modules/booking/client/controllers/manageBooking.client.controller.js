(function() {
    'use strict';

    angular
        .module('booking')
        .controller('manageBookController', manageBookController);

    manageBookController.$inject = ['$scope', 'Authentication', '$rootScope',
      '$http', '$stateParams','$timeout'];

    function manageBookController($scope, Authentication, $rootScope,
                                  $http, $stateParams, $timeout) {

      $scope.dataReceived = false;
      function stringToDate(_date, _format, _delimiter) {
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var fullMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var day = ['sunday', 'monday', 'tuesday', 'wednesday','thursday','friday','saturday'];
        var formatLowerCase = _format.toLowerCase();
        var formatItems = formatLowerCase.split(_delimiter);
        var dateItems = _date.split(_delimiter);
        var monthIndex = formatItems.indexOf("mm");
        var dayIndex = formatItems.indexOf("dd");
        var yearIndex = formatItems.indexOf("yyyy");
        var month = parseInt(dateItems[monthIndex]);
        month -= 1;
        var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
        var responseDate = {
          'dd': formatedDate.getDate(),
          'day':day[formatedDate.getDay()],
          'mm': months[formatedDate.getMonth()],
          'mmm': fullMonths[formatedDate.getMonth()],
          'yy': formatedDate.getFullYear()
        };
        return responseDate;
      }

      var params = {
        headers: { "params": JSON.stringify($stateParams) }
      };

      $http.get('/api/manageBook', params).success(function(res) {
        $scope.bookDeatils = res.Bookings;
        for(var i = 0; i < $scope.bookDeatils.length; i++){
          $scope.bookDeatils[i].check_in = stringToDate($scope.bookDeatils[i].check_in, 'dd/mm/yyyy', '/');
          $scope.bookDeatils[i].check_out = stringToDate($scope.bookDeatils[i].check_out, 'dd/mm/yyyy', '/');
          $scope.bookDeatils[i].booking_date = $scope.bookDeatils[i].booking_date.substr(0,10);
          $scope.bookDeatils[i].booking_date = stringToDate($scope.bookDeatils[i].booking_date, 'dd/mm/yyyy', '/');
        }
        $scope.allBookDeatils = angular.copy($scope.bookDeatils);
        $scope.chargingLength = (_.filter($scope.allBookDeatils, {status: "Charging"})).length;
        $scope.abortedLength = (_.filter($scope.allBookDeatils, {status: "Aborted"})).length;
        $scope.cancelledLength = (_.filter($scope.allBookDeatils, {status: "Cancelled"})).length;
        $scope.confirmedLength = (_.filter($scope.allBookDeatils, {status: "Confirmed"})).length;
        $scope.chargedLength = (_.filter($scope.allBookDeatils, {status: "Charged"})).length;
        $scope.dataReceived = true;
      });

      $scope.filter = function(type){
        $scope.bookDeatils = (type != "all") ? _.filter($scope.allBookDeatils, {status: type}) : $scope.allBookDeatils;
    }
    }


}());
