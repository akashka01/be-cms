(function() {
    'use strict';

    angular
        .module('booking')
        .controller('bookingController', bookingController);

    bookingController.$inject = ['$scope', 'Authentication', '$rootScope', '$http', 'HotelsServices', 'patterns',
      '$cookieStore', 'Calculate', '$stateParams', '$timeout', 'CLIENT'];

    function bookingController($scope, Authentication, $rootScope, $http, HotelsServices, patterns,
                               $cookieStore, Calculate, $stateParams, $timeout, CLIENT) {

      $rootScope.dataReceived = false;
      $scope.patterns = patterns;

      $rootScope.fb = {};

      HotelsServices.single($stateParams).success(function(res) {

        $timeout(function() {
          (CLIENT.name == "staydilly") ? window.scrollTo(0, 470): "";
        }, 1000);

        if (res.Hotel_Details) $rootScope.dataReceived = true;
        $scope.hotel = res.Hotel_Details[0];

        // assigning variables to send as
        // params in navigation

        $scope.searchId = res.search_id;
        $scope.productId = $stateParams.productId;
        $scope.location = $stateParams.location;
        $scope.checkin = $stateParams.checkin;
        $scope.checkout = $stateParams.checkout;
        $scope.displayCheckIn = strToDate($scope.checkin).format('DD MMM YYYY');
        $scope.displayCheckOut = strToDate($scope.checkout).format('DD MMM YYYY');

        $scope.guest = {
          rooms: $stateParams.rooms ? $stateParams.rooms : 1,
          adults: $stateParams.adults ? $stateParams.adults : 2,
          nights: getNights($scope.checkin, $scope.checkout)
        };

        function getNights(checkin, checkout) {
          var ci = strToDate(checkin);
          var co = strToDate(checkout);
          return co.diff(ci, 'days');
        }

        // format MM-DD-YYYY
        function strToDate(dt) {
          var d = dt.split('-');
          return moment([d[2], (Number(d[0]) - 1), d[1]]);
        }

        $scope.goBack = function() {
          window.history.back();
        };

        var details = {
          ratePlanId: $stateParams.ratePlanId,
          roomId: $stateParams.roomId,
          amount: $stateParams.price
        };

        function generateRooms(adults, rooms, details) {
          var output = [];

          if (adults >= rooms) {

            if (adults == rooms) {
              for (var i = 0; i < rooms; i++) {
                output.push({
                  "numOfChildren": 0,
                  "numOfAdults": 1,
                  "ratePlanId": details.ratePlanId,
                  "amount": details.amount,
                  "roomId": details.roomId
                });
              }
            } else {
              var adultRemainder = adults % rooms;
              var adultsIndividual = (adults - adultRemainder) / rooms;

              for (var i = 0; i < rooms; i++) {
                if (i == rooms - 1) {
                  output.push({
                    "numOfChildren": 0,
                    "numOfAdults": adultsIndividual + adultRemainder,
                    "ratePlanId": details.ratePlanId,
                    "amount": details.amount,
                    "roomId": details.roomId
                  });
                } else {
                  output.push({
                    "numOfChildren": 0,
                    "numOfAdults": adultsIndividual,
                    "ratePlanId": details.ratePlanId,
                    "amount": details.amount,
                    "roomId": details.roomId
                  });
                }
              }
            }
          }

          return output;
        }

        // function isDiscount(promo) {
        //     return (promo / 100);
        // }
        function isDiscount(deal, promo) {
          if (deal && !promo) {
            return (deal / 100);
          } else if (deal && promo) {
            return (deal + promo) / 100;
          } else if (!deal && promo) {
            return (promo / 100);
          } else if (!deal && !promo) {
            return 0;
          }
        }

        var netExclusive, markup, sellExclusive, discountedSellExclusive, finalPrice, finalTaxes, discount;


        var room = _.find($scope.hotel.rooms, { "room_id": Number($stateParams.roomId) });

        function calculateAll(room, guests, promo) {
          $scope.totalAmount = 0;
          $scope.subTotal = 0;
          $scope.finalTaxes = 0;
          $scope.finalTax = 0;
          $scope.discounts = 0;

          for (var i = 0; i < guests.nights; i++) {
            var taxes = room.taxes;
            var multipleRates = room.avgRates;

            var prices = {
              netInclusive: multipleRates.netRate,
              serviceTax: _.find(taxes, { 'id': 1 }) ? (_.find(taxes, { 'id': 1 }).value / 100) : 0,
              cityTax: _.find(taxes, { 'id': 3 }) ? (_.find(taxes, { 'id': 3 }).value / 100) : 0,
              gst: _.find(taxes, { 'id': 2 }) ? (_.find(taxes, { 'id': 2 }).value / 100) : 0,
              other: _.find(taxes, { 'id': 4 }) ? (_.find(taxes, { 'id': 4 }).value / 100) : 0,
              markup: (multipleRates.markupRate / 100),
              discount: isDiscount(0, promo)
              // discount: isDiscount(promo)
            };

            netExclusive = Calculate.getNetExclusive(prices.netInclusive, prices.serviceTax, prices.cityTax, prices.other, prices.gst);
            markup = Calculate.getMarkupValue(prices.netInclusive, prices.markup);
            sellExclusive = Calculate.getSellExclusive(netExclusive, markup);
            discountedSellExclusive = Calculate.getDiscountedSellExclusive(sellExclusive, prices.discount);
            finalPrice = Calculate.getFinalPrice(discountedSellExclusive, prices.serviceTax, prices.cityTax, prices.other, prices.gst);
            var finalPrices = Calculate.getFinalPrice(sellExclusive, prices.serviceTax, prices.cityTax, prices.other, prices.gst);
            finalTaxes = Calculate.getFinalTaxes(finalPrice, discountedSellExclusive);
            var finalTax = Calculate.getFinalTaxes(finalPrices, sellExclusive);
            discount = Calculate.getDiscount(sellExclusive, prices.discount);
            var discounts = Calculate.getDiscount(sellExclusive, 0);

            $scope.totalAmount += Calculate.calculateAmount(finalPrice, $scope.guest);
            $scope.subTotal += Calculate.calculateAmount(sellExclusive, $scope.guest);

            $scope.finalTaxes += Calculate.calculateAmount(finalTaxes, $scope.guest);
            $scope.finalTax += Calculate.calculateAmount(finalTax, $scope.guest);
            $scope.discounts += Calculate.calculateAmount(discount, $scope.guest);
          }
          $scope.discount = $scope.totalAmount - $scope.subTotal - $scope.finalTax;
        }

        calculateAll(room, $scope.guest);

        var promoInfo = {

          "hotelId": $scope.productId,
          "checkOutDate": strToDate($scope.checkout).format('DD/MM/YYYY'),
          "checkInDate": strToDate($scope.checkin).format('DD/MM/YYYY'),
          "promoCode": $scope.promo,
          "rooms": [{
            "roomId": $stateParams.roomId,
            "ratePlanId": $stateParams.ratePlanId
          }],
          "totalPrice": Math.round($scope.totalAmount)
        };

        var calculatePercentage = function(discoountValue, totalValue){
          return (discoountValue * 100 / totalValue);
        };

        $scope.checkPromo = function() {
          promoInfo.promoCode = ($scope.promo).toUpperCase();
          $scope.checkingPromo = true;
          $scope.promoMessage = '';

          $timeout(function() {
            $http.post('/api/validatePromo', promoInfo).success(function(res) {

              $scope.checkingPromo = false;
              if (res && res.Result) {
                $scope.promoApplied = true;
                var promo = (res.Result.PromoValue != undefined) ? res.Result.PromoValue : calculatePercentage(res.Result["Discount Price"], $scope.totalAmount);
                calculateAll(room, $scope.guest, promo);
              } else {
                $scope.promoMessage = "Invalid Promo Code, Please check and try again!";
                $scope.promoApplied = false;
                console.log(res.Error.ErrorMessage);
              }
            });
          }, 1000);

        };


        // if user is logged in auto fill
        $scope.chLogin = $cookieStore.get('validUser');
        if ($scope.chLogin) {
          var userID = {
            "userId": $cookieStore.get('userId')
          };

          var params = {
            headers: { "params": JSON.stringify(userID) }
          };
          $http.get('/api/getWalletDetails', params).success(function(res) {
            var userDeatails = res;
            $scope.firstName = userDeatails.first_name;
            $scope.lastName = userDeatails.last_name;
            $scope.emailId = userDeatails.email;
            $scope.mobileNo = userDeatails.mobile;


          });
        }

        var body = {
          "status": "booking",
          "specialRequest": $scope.spl_requests,
          "searchId": $scope.searchId,
          "checkOutDate": strToDate($scope.checkout).format('DD/MM/YYYY'),
          "hotelId": $scope.productId,
          "totalPax": $scope.guest.adults,
          "isPgRequired": true,
          "address": $scope.address,
          "checkInDate": strToDate($scope.checkin).format('DD/MM/YYYY'),
          "rooms": generateRooms(Number($scope.guest.adults), Number($scope.guest.rooms), details)
        };

        $scope.getRandomSpan = function() {
          return Math.floor((Math.random() * 6) + 1);
        };

        var getCurrencyId = function(currencyCode){
               if (currencyCode == "AUD") {return 24;}
          else if (currencyCode == "EUR") {return 3;}
          else if (currencyCode == "IDR") {return 14;}
          else if (currencyCode == "MYR") {return 18;}
          else if (currencyCode == "SGD") {return 7;}
          else if (currencyCode == "THB") {return 5;}
          else if (currencyCode == "USD") {return 2;}
          else if (currencyCode == "MVR") {return 25;}
          else if (currencyCode == "INR") {return 1;}
        }

        $scope.options = {};
        $scope.book = function(isValid) {
          var currency = ($rootScope.currency == undefined) ? $scope.hotel.currency : $rootScope.currency;

          body.firstName = $scope.firstName;
          body.lastName = $scope.lastName;
          body.emailId = $scope.emailId;
          body.mobileNo = $scope.mobileNo;
          body.noOfRooms = $scope.guest.rooms;
          body.currency = getCurrencyId(currency);
          body.source = "website";
          body.conversionRate = ($rootScope.currency != undefined) ? $rootScope.currencyMultiplier[$scope.hotel.currency] : 1;

          _(body.rooms).forEach(function(room){
            room.amount = ($rootScope.currency != undefined) ? (room.amount*$rootScope.currencyMultiplier[$scope.hotel.currency]) : room.amount;
          });

          body.totalAmount = (($rootScope.currency == undefined) ? $scope.totalAmount : ($scope.totalAmount * $scope.currencyMultiplier[$scope.hotel.currency]));
          body.subTotal = (($rootScope.currency == undefined) ? $scope.subTotal : ($scope.subTotal * $scope.currencyMultiplier[$scope.hotel.currency]));
          body.serviceTax = (($rootScope.currency == undefined) ? $scope.finalTaxes : ($scope.finalTaxes * $scope.currencyMultiplier[$scope.hotel.currency]));
          body.discounts = (($rootScope.currency == undefined) ? $scope.discounts : ($scope.discounts * $scope.currencyMultiplier[$scope.hotel.currency]));
          body.promocode = $scope.promoApplied ? $scope.promo : '';
          body.appliedPromoCode = $scope.discounts;

          body.specialRequest = $scope.spl_requests;
          body.address = $scope.address;

          if (isValid) {
            $rootScope.dataReceived = false;
            $http.post('/api/book', body).success(function(res) {
              $rootScope.dataReceived = true;
              // window.location.href = res.pgURL;
              if (res.orderid) {
                $scope.refreshed = true;
                $scope.confirmed = (res.status == "Confirmed");
                $scope.options = {
                  "mpsmerchantid": res.mcode,
                  "mpsamount": res.amount,
                  "mpsorderid": res.orderid,
                  "mpsbill_name": res.bill_name,
                  "mpsbill_email": res.bill_email,
                  "mpsbill_mobile": res.bill_mobile,
                  "mpsbill_desc": "Transaction request",
                  "mpscurrency": (($rootScope.currency == undefined) ? res.currency : $rootScope.currency),
                  "mpsvcode": res.vcode,
                  "mpsreturnurl": res.returnurl,
                  "mpscancelurl": window.location.protocol + '//' + window.location.host + '/error?orderId=' + res.orderid,
                  "startReq": true,
                  "status": true
                };

                $rootScope.fb = {
                  email: body.emailId,
                  firstName: body.firstName,
                  lastName: body.lastName,
                  phone: body.mobileNo
                };

                $scope.payment.option = ($scope.options.mpscurrency == "MYR") ? "credit" : "credit3";
                $scope.refresh();
              } else {
                alert("Something went wrong. Please try again");
              }
            });
          } else {
            console.log(isValid);
          }
        };

        $scope.$watch(function () { return $rootScope.currency }, function (obj) {
        if (obj != null && $scope.confirmed) {
          $scope.book(true);
        }
      }, true);

      });

      // $scope.trigger = function($event) {
      //     $('.myPayButton').MOLPaySeamless($scope.options);
      // };

      $scope.payment = {};
      $scope.refresh = function() {
        $scope.refreshed = false;
        $timeout(function() {
          $scope.refreshed = true;
        }, 200);
      };
    }
}());
