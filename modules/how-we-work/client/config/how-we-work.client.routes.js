(function() {
    'use strict';

    angular
        .module('howWeWork.routes')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider', 'CLIENT'];

    function routeConfig($stateProvider, CLIENT) {
        $stateProvider
            .state('howWeWork', {
                url: '/how-we-work',
                templateUrl: '/views/' + CLIENT.name + '/modules/how-we-work/client/views/how-we-work.client.view.html',
                controller: 'howWeWorkController',
                data: {
                  meta: {
                    'title': CLIENT.capitalizeName + ' | How We Work',
                    'description': 'Even the best hotels and most luxurious hotels have unsold rooms. Find out how we are able to offer you 5-star hotels at 3-star hotel prices. ',
                    'keywords': ''
                  }
                }
            });
    }
}());
