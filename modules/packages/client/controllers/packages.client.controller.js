(function() {
    'use strict';

    angular
        .module('packages')
        .controller('packagesController', packagesController);

    packagesController.$inject = ['$scope', 'Authentication', '$rootScope', 'PackagesServices', '$stateParams', '$window', '$http', '$filter', '$uibModal', '$timeout', 'Calculate', '$location', 'CLIENT'];

    function packagesController($scope, Authentication, $rootScope, PackagesServices, $stateParams, $window, $http, $filter, $uibModal, $timeout, Calculate,$location, CLIENT) {

      $timeout(function() {
        (CLIENT.name == "staydilly") ? window.scrollTo(0, 120) : "";
      }, 1000);

      $scope.$watch(function () { return $rootScope.currency }, function (obj) {
        if (obj != null) {
          $scope.currencyMultiplier = $rootScope.currencyMultiplier;
          $scope.currency = $rootScope.currency;
        }
      }, true);

      // Display the number of items in a page (Pagination)
      $scope.itemsPerPage = (CLIENT.name == "traveler.mv") ? 10 : 50;
      // Display the number of page opptions in a pagination widget
      $scope.maxSize = 3;

      $scope.totalItems = 0;
      $scope.currentPage = 1;
      $scope.priceRange = 0;
      $scope.currencies = [];
      $scope.propertyTypes = [];
      $scope.facilities = [];
      $scope.roomFacilities = [];

      $scope.pageChanged = function() {
        $scope.hotels = [];
        $scope.totalItems = $scope.allHotels.length;
        $scope.totalItem = 0;

        if ($scope.view == 'grid') {
          var itemsPer = $scope.itemsPerPage/2;
          $scope.startNum = (itemsPer * ($scope.currentPage - 1));
          $scope.endNum = ($scope.startNum + itemsPer) > $scope.totalItems ? ($scope.totalItems) : ($scope.startNum + itemsPer);
          $scope.totalItem = $scope.totalItems*2;
        } else {
          $scope.startNum = ($scope.itemsPerPage * ($scope.currentPage - 1));
          $scope.endNum = ($scope.startNum + $scope.itemsPerPage) > $scope.totalItems ? ($scope.totalItems) : ($scope.startNum + $scope.itemsPerPage);
          $scope.totalItem = $scope.totalItems;
        }

        for (var i = $scope.startNum; i < $scope.endNum; i++) {
          $scope.hotels.push($scope.allHotels[i]);
        }
      };

      $rootScope.dataReceived = false;
      PackagesServices.get($stateParams).success(function(res) {

        var listHotels, gridHotels;
        if (res) {
          $rootScope.dataReceived = true;
          $scope.searchId = res.search_id;
        }

        $scope.checkin = $stateParams.checkin;
        $scope.checkout = $stateParams.checkout;

        $scope.guest = {
          rooms: $stateParams.rooms ? $stateParams.rooms : 1,
          adults: $stateParams.adults ? $stateParams.adults : 2,
          nights: getNights($scope.checkin, $scope.checkout)
        };

        function getNights(checkin, checkout) {
          var ci = strToDate(checkin);
          var co = strToDate(checkout);
          return co.diff(ci, 'days');
        }

        function strToDate(dt) {
          var d = dt.split('-');
          return moment([d[2], (Number(d[0]) - 1), d[1]]);
        }

        if (res.Hotel_Details) {
          var hotels = res.Hotel_Details;

          for(var i = 0; i < hotels.length; i++){
            hotels[i].isAvailable = false;
            for(var j = 0; j < hotels[i].rooms.length; j++){
              if(hotels[i].rooms[j].inventory > 0) hotels[i].isAvailable = true;
            }
          }


          var filteredHotels = $filter('orderBy')(hotels, 'sellExclusive');
          $scope.minPrice = Math.min.apply(Math, filteredHotels.map(function(o) { return o.sellExclusive; }));
          $scope.maxPrice = Math.max.apply(Math, filteredHotels.map(function(o) { return o.sellExclusive; }));

          $scope.slider = {
            min: 0,
            max: $scope.maxPrice,
            options: {
              floor: $scope.minPrice.toFixed(2) - 1,
              ceil: $scope.maxPrice.toFixed(2)
            }
          };

          var currencyNames = [];
          var propertyNames = [];
          var amenityNames = [];

          for (i = 0; i < filteredHotels.length; i++) {
            if (currencyNames.indexOf(filteredHotels[i].currency) === -1) {
              currencyNames.push(filteredHotels[i].currency);
            }
            if (propertyNames.indexOf(filteredHotels[i].property_type) === -1) {
              propertyNames.push(filteredHotels[i].property_type);
            }
            for (var j = 0; j < filteredHotels[i].rooms.length; j++) {
              for (var k = 0; k < filteredHotels[i].rooms[j].amenities.length; k++) {
                if (amenityNames.indexOf(filteredHotels[i].rooms[j].amenities[k]) === -1) {
                  amenityNames.push(filteredHotels[i].rooms[j].amenities[k]);
                }
              }
            }
          }

          for (var c = 0; c < currencyNames.length; c++) {
            $scope.currencies.push({
              'name': currencyNames[c],
              'selected': false
            });
          }

          for (var c = 0; c < propertyNames.length; c++) {
            $scope.propertyTypes.push({
              'name': propertyNames[c],
              'selected': false
            });
          }

          for (var c = 0; c < amenityNames.length; c++) {
            $scope.roomFacilities.push({
              'name': amenityNames[c],
              'selected': false
            });
          }

          var names = ['Sitting Area', 'Room Service', 'Elevator', ' Outdoor Pool', 'Gymnasium',
            'Business Center', 'Restaurant', 'Wi-Fi', 'Spa', '24-Hour Front Desk',
            'Designated Smoking Area', 'Fitness Centre'
          ];
          for (var c = 0; c < names.length; c++) {
            $scope.facilities.push({
              'name': names[c],
              'selected': false
            });
          }

          listHotels = filteredHotels;
          gridHotels = chunk(filteredHotels, 2);
          $scope.totalHotels = hotels.length;
        } else {
          $scope.noHotels = true;
        }

        // covert string to title case
        function titleCase(str) { return str.toLowerCase().split(' ').map(function(val) { return val.replace(val[0], val[0].toUpperCase()); }).join(' '); }
        _(hotels).forEach(function(room) {
          calculateAll(room, $scope.guest)
        });

        function calculateAll(room, guests) {
          room.sellExclusive = 0;
          room.discountedSellExclusive = 0;
          room.Baor_rate = 0;
          for (var i = 0; i < guests.nights; i++) {

            var taxes = room.taxes;
            var multipleRates = room.avgRates;

            var prices = {
              netInclusive: multipleRates.netRate,
              serviceTax: _.find(taxes, { 'id': 1 }) ? (_.find(taxes, { 'id': 1 }).value / 100) : 0,
              cityTax: _.find(taxes, { 'id': 3 }) ? (_.find(taxes, { 'id': 3 }).value / 100) : 0,
              gst: _.find(taxes, { 'id': 2 }) ? (_.find(taxes, { 'id': 2 }).value / 100) : 0,
              other: _.find(taxes, { 'id': 4 }) ? (_.find(taxes, { 'id': 4 }).value / 100) : 0,
              markup: (multipleRates.markupRate / 100),
              discount: (room.deal.value / 100)

            };

            netExclusive = Calculate.getNetExclusive(prices.netInclusive, prices.serviceTax, prices.cityTax, prices.other, prices.gst);
            markup = Calculate.getMarkupValue(prices.netInclusive, prices.markup);
            sellExclusive = Calculate.getSellExclusive(netExclusive, markup);

            discountedSellExclusive = Calculate.getDiscountedSellExclusive(sellExclusive, prices.discount);
            finalPrice = Calculate.getFinalPrice(discountedSellExclusive, prices.serviceTax, prices.cityTax, prices.other, prices.gst);
            finalTaxes = Calculate.getFinalTaxes(finalPrice, discountedSellExclusive);
            discount = Calculate.getDiscount(sellExclusive, prices.discount);
            room.sellExclusive = sellExclusive;


            // room.sellExclusive += Calculate.calculateAmount(Math.round(sellExclusive), $scope.guest);
            room.discountedSellExclusive += Calculate.calculateAmount(Math.round(discountedSellExclusive), $scope.guest);
            room.discounts = prices.discount;
            room.Rack_rate = multipleRates.rackRate;
            if (room.Baor_rate == '0') {
              room.Baor_rate = room.Rack_rate;
            }
            if (room.Baor_rate >= room.Rack_rate) {
              room.fix_rate = room.Rack_rate;

            } else {
              room.fix_rate = room.Baor_rate;

            }


            room.Show_discount = Calculate.getdiscountpercent(sellExclusive, room.fix_rate);
          

          }

          // divide the number of nights to maintain avg rate for more nights
          room.sellExclusive = (room.sellExclusive);
          room.Baor_rate = (room.Baor_rate);
        

        }

        // assigning variables to send as
        // params in navigation
        $scope.location = $stateParams.location;
        $scope.checkin = $stateParams.checkin;
        $scope.checkout = $stateParams.checkout;
        $scope.promo = $stateParams.promo;
        $scope.guest = {
          rooms: $stateParams.rooms ? $stateParams.rooms : 1,
          adults: $stateParams.adults ? $stateParams.adults : 2
        };

        function chunk(arr, size) {
          var newArr = [];
          for (var i = 0; i < arr.length; i += size) {
            newArr.push(arr.slice(i, i + size));
          }
          return newArr;
        };
        var amenities = {
          "Clothes Rack / Wardrobe": {
            icon: '6.png',
            text: 'Clothes Rack / Wardrobe'
          },
          "Air Conditioning": {
            icon: '5.png',
            text: 'Air Conditioning'
          },
          "Carpeted": {
            icon: '7.png',
            text: 'Carpeted'
          },
          "Fan": {
            icon: '9.png',
            text: 'Fan'
          },
          "Iron": {
            icon: '12.png',
            text: 'Iron'
          },
          "Safe": {
            icon: '15.png',
            text: 'Safe'
          },
          "Television": {
            icon: '19.png',
            text: 'Television'
          },
          "Bathtub": {
            icon: '25.png',
            text: 'Bathtub'
          },
          "Shower": {
            icon: '26.png',
            text: 'Shower'
          },
          "Hairdryer": {
            icon: '29.png',
            text: 'Hairdryer'
          },
          "DVD Player": {
            icon: '41.png',
            text: 'DVD Player'
          },
          "Satellite Channels": {
            icon: '47.png',
            text: 'Satellite Channels'
          },
          "Telephone": {
            icon: '48.png',
            text: 'Telephone'
          },
          "Dining Area in Room": {
            icon: '61.png',
            text: 'Dining Area in Room'
          },
          "Electric Kettle": {
            icon: '65.png',
            text: 'Electric Kettle'
          },
          "Minibar": {
            icon: '68.png',
            text: 'Minibar'
          },
          "Kitchenette": {
            icon: 'Kitchenette.png',
            text: 'Kitchenette'
          },
          "Microwave": {
            icon: '71.png',
            text: 'Microwave'
          },
          "Refrigerator": {
            icon: '72.png',
            text: 'Refrigerator'
          },
          "Tea/Coffee Maker": {
            icon: '73.png',
            text: 'Tea/Coffee Maker'
          },
          "Coffee Machine / Espresso Machine": {
            icon: '.png',
            text: 'Coffee Machine / Espresso Machine'
          },
          "Room Service": {
            icon: '76.png',
            text: 'Room Service'
          },
          "Breakfast in the Room": {
            icon: '77.png',
            text: 'Breakfast in the Room'
          },
          "Designated Smoking Area": {
            icon: '84.png',
            text: 'Designated Smoking Area'
          },
          "Swimming Pool": {
            icon: '85.png',
            text: 'Swimming Pool'
          },
          "Gymnasium": {
            icon: '86.png',
            text: 'Gymnasium'
          },
          "Concierge Services": {
            icon: '88.png',
            text: 'Concierge Services'
          },
          "ATM On Site": {
            icon: '94.png',
            text: 'ATM On Site'
          },
          "Kid's Club": {
            icon: '114.png',
            text: "Kid's Club"
          },
          "Restaurant": {
            icon: '115.png',
            text: 'Restaurant'
          },
          "Bar": {
            icon: '117.png',
            text: 'Bar'
          },
          "BBQ Facilities": {
            icon: '121.png',
            text: 'BBQ Facilities'
          },
          "Beachfront": {
            icon: '128.png',
            text: 'Beachfront'
          },
          "Spa": {
            icon: '129.png',
            text: 'Spa'
          },
          "Disable Friendly": {
            icon: '144.png',
            text: 'Disable Friendly'
          },


          "Wifi": {
            icon: 'icon_set_1_icon-7',
            text: 'Free Wifi'
          },
          "Wi-Fi (Complimentary)": {
            icon: 'wifi.png',
            text: 'Free Wifi'
          },
          "bed": {
            icon: 'icon_set_2_icon-115',
            text: '1 Double or 2 Single Beds'
          },
          // "breakfast": {
          //     icon: 'icon_set_3_restaurant-6',
          //     text: 'Free breakfast'
          // },
          "room": {
            icon: 'icon_set_1_icon-64',
            text: '1 Double or 2 Single Rooms'
          },
          "AC": {
            icon: 'icon-air',
            text: 'Ac'
          },
          'Tea Maker': {
            icon: 'icon_set_3_restaurant-8',
            text: 'Tea/Coffee Maker'
          },
          "Phone": {
            icon: 'icon_set_1_icon-90',
            text: 'Phone'
          }
        };

        var stars = ['rating.gif', 'rating.gif', 'rating.gif', 'rating.gif', 'rating.gif'];

        $scope.amIcon = function(am) {
          return amenities[am] ? amenities[am].icon : '';
        };

        $scope.amText = function(am) {
          return amenities[am] ? amenities[am].text : '';
        };

        $scope.getStars = function(number) {
          var index = Math.round(number) - 1;
          return stars[index];
        };

        $scope.myOrder = "-Show_discount";
        $scope.changeSort = function(sort, flag){
          if($scope.view == 'list'){
            arrayOfHotels = angular.copy($scope.allHotels);
          }
          else{
            var arrayOfHotels = [];
            for(var i = 0; i < $scope.allHotels.length; i++){
              if($scope.allHotels[i][0] != undefined) arrayOfHotels.push($scope.allHotels[i][0]);
              if($scope.allHotels[i][1] != undefined) arrayOfHotels.push($scope.allHotels[i][1]);
            }
          }

          if(sort == 'price'){
            arrayOfHotels.sort(function(a, b){
              return a.sellExclusive-b.sellExclusive
            })
          }
          else if(sort == 'discount'){
            arrayOfHotels.sort(function(a, b){
              return a.Show_discount-b.Show_discount
            });
            arrayOfHotels.reverse();
          }
          else if (sort == 'recommended'){
            arrayOfHotels.sort(function(a, b){
              return a.sellExclusive-b.sellExclusive
            })
          }
          else if (sort == 'name'){
            arrayOfHotels.sort(function(a, b){
              if (a.hotel_name < b.hotel_name) //sort string ascending
                  return -1;
              if (a.hotel_name > b.hotel_name)
                  return 1;
              return 0;
            })
          } 

          // Reverse the array if flag = true
          if(flag == true){
            arrayOfHotels.reverse();
          }

          gridHotels = chunk(arrayOfHotels, 2);
          listHotels = arrayOfHotels;
          $scope.changeView($scope.view, false);
        };

        $scope.changeView = function(view, flag) {
          $scope.view = view;
          if (view == 'grid') {
            $scope.allHotels = gridHotels;
          } else {
            $scope.allHotels = listHotels;
          }
          $scope.hotelsList = [];
          $scope.hotelsList = angular.copy($scope.allHotels);
          $scope.pageChanged();

          if(flag) $scope.onFilterChange();
        };

        $scope.changeView('list', false);
        $scope.changeSlide = function($event) {
          $event.preventDefault();
          $event.stopPropagation();
        };
        // Carousel
        $scope.myInterval = 5000;
        $scope.noWrapSlides = false;
        $scope.active = 0;

        $scope.onPriceChange = function() {
          for (var h = 0; h < $scope.allHotels.length; h++) {
            if ($scope.view == 'list') {
              if ($scope.allHotels[h].sellExclusive < $scope.slider.min || $scope.allHotels[h].sellExclusive > $scope.slider.max) {
                $scope.allHotels.splice(h, 1);
                h--;
              }
            }
            else{
              for(var ch = 0; ch < $scope.allHotels[h].length; ch++){
                if ($scope.allHotels[h][ch].sellExclusive < $scope.slider.min || $scope.allHotels[h][ch].sellExclusive > $scope.slider.max) {
                  $scope.allHotels[h].splice(ch,1);
                  ch--;
                }
              }
              if($scope.allHotels[h].length <= 0){
                $scope.allHotels.splice(h,1);
                h--;
              }
            }
          }
        };

        $scope.onPropertyTypeChange = function() {
          var toDo = false;
          for (var j = 0; j < $scope.propertyTypes.length; j++) {
            if ($scope.propertyTypes[j].selected) {
              toDo = true;
              break;
            }
          }

          if (toDo) {
            for (var i = 0; i < $scope.allHotels.length; i++) {
              var isFound = true;
              if ($scope.view == 'list') {
                for (var j = 0; j < $scope.propertyTypes.length; j++) {
                  if (!$scope.propertyTypes[j].selected && $scope.propertyTypes[j].name == $scope.allHotels[i].property_type) {
                    isFound = false;
                  }
                }
                if (!isFound) {
                  $scope.allHotels.splice(i, 1);
                  i--;
                }
              }
              else{
                for(var ch = 0; ch < $scope.allHotels[i].length; ch++){
                  isFound = true;
                  for (var j = 0; j < $scope.propertyTypes.length; j++) {
                    if ($scope.propertyTypes[j].selected && $scope.propertyTypes[j].name === $scope.allHotels[i][ch].property_type) {
                      isFound = false;
                    }
                  }
                  if (isFound) {
                    $scope.allHotels[i].splice(ch, 1);
                    ch--;
                  }
                }
                if($scope.allHotels[i].length <= 0){
                  $scope.allHotels.splice(i,1);
                  i--;
                }
              }
            }
          }
        };

        $scope.onHotelSearch = function() {
          if ($scope.searchedHotel != "" && $scope.searchedHotel != undefined) {
            for (var i = 0; i < $scope.allHotels.length; i++) {
              if ($scope.view == 'list') {
                if ($scope.allHotels[i].hotel_name.indexOf(titleCase($scope.searchedHotel)) === -1) {
                  $scope.allHotels.splice(i, 1);
                  i--;
                }
              }
              else{
                for(var ch = 0; ch < $scope.allHotels[i].length; ch++){
                  if ($scope.allHotels[i][ch].hotel_name.indexOf(titleCase($scope.searchedHotel)) === -1) {
                    $scope.allHotels[i].splice(ch, 1);
                    ch--;
                  }
                }
                if($scope.allHotels[i].length <= 0){
                  $scope.allHotels.splice(i,1);
                  i--;
                }
              }
            }
          }
        };

        $scope.onRoomFacilityChange = function() {
          var toDo = false;
          for (var j = 0; j < $scope.roomFacilities.length; j++) {
            if ($scope.roomFacilities[j].selected) {
              toDo = true;
              break;
            }
          }

          if (toDo) {
            for (var i = 0; i < $scope.allHotels.length; i++) {
              var isFound = false;
              if ($scope.view == 'list'){
                for (var j = 0; j < $scope.roomFacilities.length; j++) {
                  if($scope.allHotels[i].rooms != undefined && $scope.allHotels[i].rooms.length > 0){
                    for (var m = 0; m < $scope.allHotels[i].rooms.length; m++) {
                      if ($scope.roomFacilities[j].selected && $scope.allHotels[i].rooms[m].amenities.indexOf(amenityNames[j]) != -1) {
                        isFound = true;
                      }
                    }
                  }
                }
                if (!isFound) {
                  $scope.allHotels.splice(i, 1);
                  i--;
                }
              }
              else{
                for(var ch = 0; ch < $scope.allHotels[i].length; ch++){
                  for (var j = 0; j < $scope.roomFacilities.length; j++) {
                    for (var m = 0; m < $scope.allHotels[i][ch].rooms.length; m++) {
                      if ($scope.roomFacilities[j].selected && $scope.allHotels[i][ch].rooms[m].amenities.indexOf(amenityNames[j]) != -1) {
                        isFound = true;
                      }
                    }
                  }
                  if (!isFound) {
                    $scope.allHotels[i].splice(ch, 1);
                    ch--;
                  }
                }
                if($scope.allHotels[i].length <= 0){
                  $scope.allHotels.splice(i, 1);
                  i--;
                }
              }
            }
          }
        };

        $scope.onFacilityChange = function() {
          var toDo = false;
          for (var j = 0; j < $scope.facilities.length; j++) {
            if ($scope.facilities[j].selected) {
              toDo = true;
              break;
            }
          }

          if (toDo) {
            for (var i = 0; i < $scope.allHotels.length; i++) {
              var isFound = false;
              if ($scope.view == 'list'){
                for (var j = 0; j < $scope.facilities.length; j++) {
                  if ($scope.facilities[j].selected && $scope.allHotels[i].amenities.indexOf(names[j]) != -1) {
                    isFound = true;
                  }
                }
                if (!isFound) {
                  $scope.allHotels.splice(i, 1);
                  i--;
                }
              }
              else{
                for(var ch = 0; ch < $scope.allHotels[i].length; ch++){
                  for (var j = 0; j < $scope.facilities.length; j++) {
                    if ($scope.facilities[j].selected && $scope.allHotels[i][ch].amenities.indexOf(names[j]) != -1) {
                      isFound = true;
                    }
                  }
                  if (!isFound) {
                    $scope.allHotels[i].splice(ch, 1);
                    ch--;
                  }
                }
                if($scope.allHotels[i].length <= 0){
                  $scope.allHotels.splice(i, 1);
                  i--;
                }
              }
            }
          }
        };

        $scope.mergeArray = function(){
          var tempListing = [];
          for(var i = 0; i < $scope.allHotels.length; i++){
            if($scope.allHotels[i].length === 1){
              tempListing.push($scope.allHotels[i]);
              $scope.allHotels.splice(i,1);
              i--;
            }
          }
          for(var i = 0; i < tempListing.length; (i=i+2)){
            if(tempListing[i+1] != undefined) tempListing[i].push(tempListing[i+1][0]);
            $scope.allHotels.push(tempListing[i]);
          }
        };

        $scope.onFilterChange = function() {
          $scope.allHotels = angular.copy($scope.hotelsList);
          $scope.onPriceChange();
          $scope.onHotelSearch();
          $scope.onPropertyTypeChange();
          $scope.onRoomFacilityChange();
          $scope.onFacilityChange();
          if($scope.view == 'grid') $scope.mergeArray();
          if ($scope.allHotels.length > 0) $scope.pageChanged();
        };

        $scope.$watch(function () { return $scope.slider.min }, function (obj) {
          if (obj != null) {
            $scope.onFilterChange();
          }
        }, true);

        $scope.$watch(function () { return $scope.slider.max }, function (obj) {
          if (obj != null) {
            $scope.onFilterChange();
          }
        }, true);


        $http.get('/api/getLocations').success(function(res) {

          $scope.loca = res.locations;

        });

        $scope.openMapModal = function($event, hotel) {
          $event.preventDefault();
          $event.stopPropagation();
          var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'views/staydilly/modules/users/client/views/modals/maps.client.html',
            controller: 'mapsController',
            controllerAs: '$ctrl',
            resolve: {
              hotel: function() {
                return hotel;
              }
            }
          });
        };
      });
    }
    }());
