(function(app) {
    'use strict';

    app.registerModule('packages', ['core']); // The core module is required for special route handling; see /core/client/config/core.client.routes
    app.registerModule('packages.services');
    app.registerModule('packages.routes', ['ui.router', 'packages.routes', 'packages.services']);
}(ApplicationConfiguration));
