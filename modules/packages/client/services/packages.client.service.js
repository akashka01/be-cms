(function () {
  'use strict';

  angular
    .module('packages.services')
    .factory('PackagesServices', PackagesServices);

  PackagesServices.$inject = ['$resource', '$log', '$http'];

  function PackagesServices($resource, $log, $http) {
    return {
      get: function(params) {
        return $http.get('/api/getPackages', {
          headers: {
            "params": JSON.stringify(params)
          }
        });
      },
      single: function(params) {
        return $http.get('/api/getSinglePackage', {
          headers: {
            "params": JSON.stringify(params)
          }
        });
      }
    }
  }

}());
