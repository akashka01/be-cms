(function() {
    'use strict';

    angular
        .module('packages.routes')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider', 'CLIENT'];

    function routeConfig($stateProvider, CLIENT) {
        $stateProvider
            .state('packages', {
                url: '/packages?location?stateId?country?checkin?checkout?rooms?adults?deal?propertyType?promo',
                templateUrl: '/views/' + CLIENT.name + '/modules/packages/client/views/packages.client.view.html',
                controller: 'packagesController',
                controllerAs: 'vm'
            })
            .state('viewpackage', {
                url: '/packages/:url?searchId?productId?location?checkin?checkout?rooms?adults?promo',
                templateUrl: '/views/' + CLIENT.name + '/modules/packages/client/views/ViewPackage.client.view.html',
                controller: 'viewPackageController',
                controllerAs: 'vm'
              })
    }
}());
