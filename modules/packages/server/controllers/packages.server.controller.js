'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Package = mongoose.model('Packages'),
  request = require('request'),
  _ = require('lodash'),
  moment = require('moment'),
  projectConfig = require(path.resolve('./modules/core/server/config/config')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

var config = projectConfig();
var headers = {
  apiKey: config.apiKey,
  channelId: config.channelId
};

function formPaxInfo(adults, rooms) {
  var paxInfo = '&paxInfo=';
  if (adults == rooms) {
    for (var i = 0; i < rooms; i++) {
      paxInfo += (i == rooms - 1) ? ('1|0') : ('1|0||');
    }
  } else if (adults > rooms){
    var adultRemainder = adults % rooms;
    var adultsIndividual = (adults - adultRemainder) / rooms;
    for (var i = 0; i < rooms; i++) {
      paxInfo += (i == rooms - 1) ? ((adultsIndividual + adultRemainder) + '|0') : (paxInfo += adultsIndividual + '|0||');
    }
  }
  return paxInfo;
}

exports.create = function(req, res) {
  /*var package = new Package(req.body);
  package.save(function(err) {
    if (err) return res.status(400).send(err);
    else res.jsonp(package);
  })*/
};

exports.update = function(req, res) {
  /*var package = req.package;
  package = _.extend(package, req.body);
  package.save(function(err) {
    if (err) return res.status(400).send(err);
    else res.jsonp(package);
  });*/
};

exports.packageByID = function(req, res, next, id) {
 /* if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({ message: 'Package is invalid' })
  }
  var query = Package.findById(id);
  query.exec(function(err, hotel) {
    if (err) return next(err);
    else if (!package) return res.status(404).send(err);
    req.package = package;
    next();
  });*/
};

exports.getPackages = function(req, res) {
  var params = req.headers.params ? JSON.parse(req.headers.params) : {};
  var param = {
    location: params.location ? '&cityId=' + params.location : '',
    stateId: params.stateId ? '&stateId=' + params.stateId : '',
    country: params.country ? '&countryId=' + params.country : '',
    checkin: params.checkin ? '&checkIn=' + moment(new Date(params.checkin)).format('DD/MM/YYYY') : '&checkIn=' + moment().add(1, 'd').format('DD/MM/YYYY'),
    checkout: params.checkout ? '&checkOut=' + moment(new Date(params.checkout)).format('DD/MM/YYYY') : '&checkOut=' + moment().add(2, 'd').format('DD/MM/YYYY'),
    paxInfo: params.adults && params.rooms ? formPaxInfo(Number(params.adults), Number(params.rooms)) : '',
    propertyType: params.propertyType ? '&propertyTypeId=' + params.propertyType : '',
    dealId: params.deal ? '&dealId=' + params.deal : '',
    promoId: params.promo ? '&promoId=' + params.promo : ''
  };

  // Configure the request
  var options = {
    url: config.url + config.packageSearchApi + config.beId + param.location + param.stateId + param.country + param.checkin + param.checkout + param.paxInfo +
    param.propertyType + param.dealId + param.promoId,
    method: 'GET',
    headers: headers
  };

  // Start the request
  request(options, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      res.json(JSON.parse(body));
    }
  })
};

exports.getSinglePackage = function(req, res) {
  var params = JSON.parse(req.headers.params);
  var param = {
    searchId: '&searchId=' + params.searchId,
    productId: '&productId=' + params.productId,
    checkIn: params.checkin ? '&checkIn=' + moment(new Date(params.checkin)).format('DD/MM/YYYY') : '&checkIn=' + moment().add(1, 'd').format('DD/MM/YYYY'),
    checkOut: params.checkout ? '&checkOut=' + moment(new Date(params.checkout)).format('DD/MM/YYYY') : '&checkOut=' + moment().add(2, 'd').format('DD/MM/YYYY')
  };

  // Configure the request
  var options = {
    url: config.url + config.packageApi + config.beId + param.searchId + param.productId + param.checkIn + param.checkOut,
    method: 'GET',
    headers: headers
  };

  // Start the request
  request(options, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      res.json(JSON.parse(body));
    }
  })
};
