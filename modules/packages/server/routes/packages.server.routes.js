'use strict';

/**
 * Module dependencies
 */
var packagesPolicy = require('../policies/packages.server.policy'),
  packages = require('../controllers/packages.server.controller');

module.exports = function (app) {

  app.route('/api/getPackages')
    .get(packages.getPackages);

  app.route('/api/getSinglePackage')
    .get(packages.getSinglePackage);

  app.route('/api/packages')
    .post(packages.create);

  app.route('/api/packages/:packageId')
    .put(packages.update);

  app.param('packageId', packages.packageByID);

};
